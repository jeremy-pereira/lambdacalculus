//
//  Expression.swift
//  
//
//  Created by Jeremy Pereira on 02/02/2021.
//
//  Copyright (c) Jeremy Pereira 2021, 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox

private let log = Logger.getLogger("LambdaKit.Expression")

/// Objects adopt this protocol to be stringifiable as a lambda expression
///
/// All `Expressible` objects must be stringifiable in order to print them out
/// in a human readable way.
public protocol Stringifiable
{
	/// Turn into a string suitable for printing
	///
	/// - Parameter useLabel: if true and the expression derives directly from
	/// a `let` expression, print the name of the `let` instead of the expression.
	func stringify(useLabel: Bool) -> String

	/// Does the expression need parentheses to keep it together
	///
	/// Abstractions and applications appearing in larger expressions often need
	/// to be parenthesised to make the order of evaluation unambiguous. This
	/// function tells us if parentheses will be needed for an arbitrary
	/// expression.
	/// - Parameter useLabel: if true and the expression derives directly from
	/// a `let` expression, we will return false because when stringifying, the
	/// label will be used, not the whole expression.
	/// - Returns: true if the expression is constructed from sub expressions
	func isMultipart(useLabel: Bool) -> Bool

	/// Label of expression
	///
	/// If the expression was defined in a `let`, it will have a label which is
	/// the name on the left side of the `let` expression.
	var label: String? { get set }
}

/// Adopted by objects that can be beta reduced
///
/// β reduction is the process of applying a function in a lambda expression to
/// its argument. The ``Application`` is replaced by the body of its ``Abstraction``
/// after all occurences of the body's bound variable have been replaced with
/// the application's argument. For example
/// ```
/// (λx.x y z) a ->β a y z
/// ```
/// In the above `(λx.x y z) a` is an application. The application's abstraction
/// (or function) is `(λx.x y z)`. Its argument is `a`. The first step is to
/// take the body of the abstraction `(x y z)` and replace occurences of the
/// bound variable - `x` with the argument `a` giving us `a y z`. This then
/// replaces the application.
public protocol BetaReducible
{
	/// Beta reduce the expression
	/// - Parameter context: Context determining how numerals are handled etc
	/// - Returns: The beta reduced expression or `nil` if there were no beta
	///            reductions found
	func betaReducedOrNil(context: ReductionContext) -> Expressible?

	/// Substitute parameters for a beta reduction
	/// - Parameters:
	///   - parameter: The parameter to substitute
	///   - expression: The expression with which to substitute it
	///   - context: Context for handling numerals etc
	/// - Returns: The expression with the parameters substituted or `nil` if none were made
	func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?

	/// The number of redexes in this beta reducible
	var redexCount: Int { get }
}

public extension Expressible
{

	/// Beta reduction that will give us a definitie expression
	/// - Parameter context: Context for handling numerals
	/// - Returns: The beta reduced expression or self if no reduction happened
	func betaReduced(context: ReductionContext) -> Expressible
	{
		return self.betaReducedOrNil(context: context) ?? self
	}
}

/// Adopted by objects that can be alpha reduced
///
/// Two expressions are alpha equivalent if the only thing that stops them
/// being equal is the bound variable names.  For example `λx.x y z` is alpha
/// equivalent to `λa.a y z`
///
/// The name of the bound variable in an abstraction is not all that important
/// and so α-reduction involves substituting a bound variable with a new one of
/// a different name resulting in an α equivalent expression.
///
public protocol AlphaReducible
{
	/// Find the alpha reduction of an expression
	///
	/// The name of the bound variable in an abstraction is not all that important
	/// and so α-reduction involves substituting a bound variable with a new one of
	/// a different name resulting in an α equivalent expression.
	/// - Parameters:
	///   - old: parameter to be substitue
	///   - new: parameter with which to substitute the old parameter
	func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible

	/// Test an expression or subexpression for alpha equivalence
	///
	/// Two expressions are alpha equivalent if the only thing that stops them
	/// being equal is the bound variable names.  For example `λx.x y z` is alpha
	/// equivalent to `λa.a y z`
	/// - Parameters:
	///   - rhs: The expression to compare to
	///   - bindingEquivalences: List of equivalent parameters descovered higher
	///                          up in the expression tree
	func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
}

public extension AlphaReducible
{
	/// Tests if an expression is alpha equivalent to another expression.
	///
	/// Two expressions are alpha equivalent if the only thing that stops them
	/// being equal is the bound variable names.
	///
	/// For example `λx.x y z` is alpha equivalent to `λa.a y z`
	/// - Parameter rhs: The expression to test alpha equivalence of
	/// - Returns: True if two expressions are alpha equivalent
	func isAlphaEquivalent(to rhs: Expressible) -> Bool
	{
		return isAlphaEquivalent(to: rhs, bindingEquivalences: [])
	}
}

/// Models a lambda expression.
///
/// There are three types of Lambda expression
/// - *variables*
/// - *abstractions* of the form `λx.M` where x is a variable and M is a lambda expression
/// - *applications* of the form `M N` where M and N are both Lambda expressions.
public protocol Expressible: AnyObject, Stringifiable, BetaReducible, AlphaReducible, Encodable
{

	/// Tests equality for this expression to another expression
	///
	/// Two expressions are equal if
	/// - they are variables of any type and they have the same name, \
	/// - if they are applications and their sub expressions are equal
	/// - if they are abstractions if their sub expressions are equal and their
	///   parameters have the same name.
	/// - Parameter to: expression to compare to
	/// - Returns: true if the expressions are equal
	func isEqual(to: Expressible) -> Bool

	/// Get the names of all the free variables associated with the expression
	///
	/// - TODO: change to get the actual free variables and use an extension
	///         function to get the names.
	var freeVariableNames: Set<String> { get }

	/// Count the number of ocurrences of a free variable in an expression
	/// - Parameter freeVariable: The free variable to count
	func count(freeVariable: FreeVariable) -> Int

	/// The expression cast as an abstraction
	///
	/// Returns `nil` if the expression is not an abstraction.
	var asAbstraction: Abstraction? { get }
}

/// Test for equality
///
/// I'm not using `Equatable` at this time because that introduces `Self`
/// constraints which makes for an explosion of generics.
/// - Parameters:
///   - lhs: First expression to be compared
///   - rhs: second expression to be compared
/// - Returns: true if both expressions represent the same lambda expression
public func ==(lhs: Expressible, rhs: Expressible) -> Bool
{
	return lhs.isEqual(to: rhs)
}

/// Test for inequality
///
/// I'm not using `Equatable` at this time because that introduces `Self`
/// constraints which makes for an explosion of generics.
/// - Parameters:
///   - lhs: First expression to be compared
///   - rhs: second expression to be compared
/// - Returns: true if the expressions represent different lambda expression
public func !=(lhs: Expressible, rhs: Expressible) -> Bool
{
	return !lhs.isEqual(to: rhs)
}

/// Context to change the behiour of beta reductions
public struct ReductionContext
{

	/// True if we are supposed to be collapsing numerals
	public let collapseNumerals: Bool

	public init(collapseNumerals: Bool)
	{
		self.collapseNumerals = collapseNumerals
	}
}

/// Protocol to whch all Lambda abstractions must conform
///
///
/// An abstraction is the Lambda Calculus representation of a function.
/// An abstraction has a parameter and a body. The parameter is said to be
/// bound in the body.
public protocol Abstraction: Expressible
{
	/// Substitute an expression for the bound variable and rteturn the body
	/// - Parameter argument: The expression to replace the bound variable with
	/// - Parameter context: Describes various non standard behaviours that might need applying
	/// - Returns: A new Lambda expression created by substituting the body with the given expression
	func substituting(argument: Expressible, context: ReductionContext) -> Expressible
}

/// Protocol to which anything representing a variable must conform
public protocol Variable: Expressible
{

}


/// Parameter for an abstraction
///
/// The parameter is just a placeholder and so not actually an expession in
/// itself, but it does need a unique reference, so we can't use a string
public class Parameter: Encodable
{
	/// The current name of the  parameter
	///
	/// This is only a hint for display purposes and it may change if there is a clash with a free
	/// variable.
	public internal(set) var name: String

	/// Initialise a parameter
	/// - Parameter name: The name of the parameter
	public init(_ name: String)
	{
		self.name = name
	}

	/// Initialsie a parameter from a free variable
	/// - Parameter freeVariable: The free variable from which to get the name
	convenience init(freeVariable: FreeVariable)
	{
		self.init(freeVariable.name)
	}

	/// Make the name of this parameter unique compared to a set of names
	///
	/// The name is uniquified by adding primes to it.
	/// - Parameter usedNames: A set of names compared to which the name of
	///                        this parameter must be unique
	fileprivate func uniqueTheName(usedNames: Set<String>)
	{
		while usedNames.contains(name)
		{
			name += "'"
		}
	}


	private enum CodingKeys: String, CodingKey
	{
		case name
		case identifier
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(name, forKey: .name)
		try container.encode(ObjectIdentifier(self).debugDescription, forKey: .identifier)
	}

}
/// Lambda free variable
///
/// This is used for free variables only.
public class FreeVariable: Variable
{
	/// Free variables are always "atomic"
	/// - Parameter useLabel: ignored
	/// - Returns: false
	public func isMultipart(useLabel: Bool) -> Bool { false }

	/// Initialise a free variable
	/// - Parameter name: The name of the free variable
	public init(_ name: String)
	{
		self.name = name
	}
	/// The name of the free variable
	public let name: String
	/// Label of a variable is meaningless really
	public var label: String? = nil

	public func stringify(useLabel: Bool) -> String
	{
		label ?? name
	}

	public func isEqual(to rhs: Expressible) -> Bool
	{
		guard let rhs = rhs as? FreeVariable else { return false }
		return self.name == rhs.name
	}

	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		return self == rhs
	}

	public func betaReducedOrNil(context: ReductionContext) -> Expressible? { nil }

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		nil
	}

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		self
	}

	/// The free variable names of a free variable is just the name of this free variable
	public var freeVariableNames: Set<String> { [name] }

	public func encode(to encoder: Encoder) throws
	{
		try name.encode(to: encoder)
	}

	public func count(freeVariable: FreeVariable) -> Int
	{
		return self == freeVariable ? 1 : 0
	}

	public var asAbstraction: Abstraction? { nil }

	public let redexCount = 0
}
//
/// Represents a bound variable within an abstraction.
///
/// The only thing that really matters with bound variables is which
/// abstraction they are bound to. The actual name is irrelevant and can
/// be changed without changing the abstraction, as long as all instances
/// of the same bound variable are changed.
public class BoundVariable: Variable
{
	public var label: String?

	public var freeVariableNames: Set<String>  { Set<String>() }

	public func isMultipart(useLabel: Bool) -> Bool { false }

	public func stringify(useLabel: Bool) -> String
	{
		label ?? parameter.name
	}

	fileprivate let parameter: Parameter

	/// Initialise a bound variable
	/// - Parameter parameter: The parameter to which the variable is bound
	public init(_ parameter: Parameter)
	{
		self.parameter = parameter
	}

	public func isEqual(to rhs: Expressible) -> Bool
	{
		guard let rhs = rhs as? BoundVariable else { return false }
		return self.parameter === rhs.parameter
	}

	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		guard let rhs = rhs as? BoundVariable else { return false }
		guard let (_, expectedRhsFreeVar) = bindingEquivalences.first(where: { $0.0 === self.parameter} )
		else { fatalError("Bound variable has no equivalence in binding equivalences") }
		return rhs.parameter === expectedRhsFreeVar
	}

	public func betaReducedOrNil(context: ReductionContext) -> Expressible? { nil }

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		return parameter === self.parameter ? expression : nil
	}

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		return old !== parameter ? self : BoundVariable(new)
	}

	private enum CodingKeys: String, CodingKey
	{
		case bound
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(parameter, forKey: .bound)
	}

	public func count(freeVariable: FreeVariable) -> Int { 0 }

	public var asAbstraction: Abstraction? { nil }

	public let redexCount = 0
}
/// Lambda Calculus Abstraction
///
/// An abstraction is the Lambda Calculus representation of a function.
/// An abstraction has a parameter and a body. The parameter is said to be
/// bound in the body.
public class ConcreteAbstraction: Abstraction
{
	public var label: String?

	fileprivate let parameter: Parameter
	private let body: Expressible

	/// Creates an abstraction with a parameter and expression
	///
	/// Note that this methods will not make any links with variables in the
	/// expression. For variables in the expression to be bound, they need to
	/// be set to the parameter already.
	/// - Parameters:
	///   - parameter: The bind parameter
	///   - expression: The expression for the abstraction.
	private init(parameter: Parameter, in expression: Expressible)
	{
		self.parameter = parameter
		self.body = expression
		self.redexCount = body.redexCount
	}

	public var redexCount: Int

	/// Creates an abstraction with a parameter name and expression
	///
	/// Note that this methods will not make any links with variables in the
	/// expression.
	/// - Parameters:
	///   - parameterName: The bind parameter name
	///   - expression: The expression for the abstraction.
	/// - Returns: A new ``Abstraction``
	public static func create(parameterName: String, in expression: Expressible, context: ReductionContext = ReductionContext(collapseNumerals: false))
	-> Abstraction
	{
		let variable = Parameter(parameterName)
		return create(parameter: variable, in: expression, context: context)
	}

	/// Creates an abstraction with a parameter and expression
	///
	/// Use this function rather than `init` because it can check if the
	/// abstraction is a ``Numeral`` and return one instead of a a `ConcreteAbstraction`
	///
	/// Note that this methods will not make any links with variables in the
	/// expression. For variables in the expression to be bound, they need to
	/// be set to the parameter already.
	/// - Parameters:
	///   - parameter: The bind parameter
	///   - expression: The expression for the abstraction.
	/// - Returns: A new ``Abstraction``
	public static func create(parameter: Parameter, in expression: Expressible, context: ReductionContext = ReductionContext(collapseNumerals: false))
	-> Abstraction
	{
		if let inner = expression as? ConcreteAbstraction, context.collapseNumerals
		{
			let fParam = parameter
			let xParam = inner.parameter
			var bodyToCheck = inner.body
			var count = 0
			// A numeral has a recursive chain of applications of f that ends with
			// a bare x. Traverse the chain until we get to the bare x or something
			// that doesn't fit the pattern
			while let application = bodyToCheck as? Application,
				  let appFunction = application.function as? BoundVariable,
				  appFunction.parameter === fParam
			{
				count += 1
				bodyToCheck = application.argument
			}
			// At this point, we have traversed to the end of the chain and
			// should have a bare bound x. If not, it's not a numeral
			if let lastArgument = bodyToCheck as? BoundVariable,
			   lastArgument.parameter === xParam
			{
				return Numeral(value: count)
			}
		}
		// If we get here, we will not be creating a numeral

		return ConcreteAbstraction(parameter: parameter, in: expression)
	}


	public func isMultipart(useLabel: Bool) -> Bool
	{
		return !useLabel || label == nil
	}

	public func stringify(useLabel: Bool) -> String
	{
		if let label = label, useLabel { return label }

		parameter.uniqueTheName(usedNames: freeVariableNames)
		return "λ\(parameter.name).\(body.stringify(useLabel: true))"
	}

	public func isEqual(to rhs: Expressible) -> Bool
	{
		guard let rhs = rhs as? ConcreteAbstraction else { return false }
		return self.parameter.name == rhs.parameter.name
			&& self.body.isEqual(to: rhs.body)
	}

	// TODO: Needs to be fixed so that we can compare to numerals
	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		if let rhs = rhs as? ConcreteAbstraction
		{
			return body.isAlphaEquivalent(to: rhs.body,
										  bindingEquivalences: bindingEquivalences + [(self.parameter, rhs.parameter)])
		}
		else if let rhs = rhs as? Numeral
		{
			return rhs.isAlphaEquivalent(to: self, bindingEquivalences: bindingEquivalences)
		}
		else if let rhs = rhs as? PrimitiveAbstraction
		{
			return rhs.isAlphaEquivalent(to: self, bindingEquivalences: bindingEquivalences)
		}
		else
		{
			return false
		}
	}

	public func substituting(argument: Expressible, context: ReductionContext) -> Expressible
	{
		let ret = body.substituting(parameter: parameter, with: argument, context: context) ?? body
		log.debug("Applying function: \(self.stringify(useLabel: false)), argument: \(argument.stringify(useLabel: false)) -> \(ret.stringify(useLabel: false))")
		return ret
	}

	public func betaReducedOrNil(context: ReductionContext) -> Expressible?
	{
		guard redexCount > 0 else { return nil }
		guard let newBody = body.betaReducedOrNil(context: context) else { return nil }
		return ConcreteAbstraction.create(parameter: self.parameter, in: newBody, context: context)
	}

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		// It's possible to create an abstraction with a recursive call, in
		// which case, our parameter "shadows" the higher level version and
		// we need to do nothing
		guard parameter !== self.parameter else { return nil }

		guard let newBody = body.substituting(parameter: parameter, with: expression, context: context)
		else { return nil }
		let newParam = Parameter(self.parameter.name)
		return ConcreteAbstraction.create(parameter: newParam,
										  in: newBody.alphaReduced(substituting: self.parameter, with: newParam),
										  context: context)
	}

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		//
		// If the old parameter is the same as our parameter, this must be
		// a recursive call. Our version of old is shadowing the higher
		// level one, so we ignore the alpha reduction in this case.
		guard old !== self.parameter else { return self }

		let newBody = body.alphaReduced(substituting: old, with: new)
		if newBody === body
		{
			return self
		}
		else
		{
			return ConcreteAbstraction(parameter: parameter, in: newBody)
		}
	}

	private enum CodingKeys: String, CodingKey
	{
		case parameter = "_lambda"
		case redexCount
		case body
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(parameter, forKey: .parameter)
		try container.encode(Expression.CodingWrapper(body), forKey: .body)
		try container.encode(redexCount, forKey: .redexCount)
	}


	public var freeVariableNames: Set<String> { body.freeVariableNames }

	public func count(freeVariable: FreeVariable) -> Int
	{
		body.count(freeVariable: freeVariable)
	}

	public var asAbstraction: Abstraction? { self }
}
//
/// Application expression
///
/// An application has two parts: a function and an argument. If the function is
/// and ``Abstraction`` then the application is a "redex" and the argument can
/// be applied to it.
public class Application: Expressible
{
	public var label: String?
	
	fileprivate let function: Expressible
	fileprivate let argument: Expressible

	/// Initialise an Application
	/// - Parameters:
	///   - function: /the function of the application
	///   - argument: The function's argument
	public init(apply function: Expressible, to argument: Expressible)
	{
		self.function = function
		self.argument = argument
		self.redexCount = function.redexCount + argument.redexCount + (function.asAbstraction != nil ? 1 : 0)
	}

	public var redexCount: Int

	public func isMultipart(useLabel: Bool) -> Bool { true }

	public func stringify(useLabel: Bool) -> String
	{
		if let label = label, useLabel { return label }

		return Application.parenthesize(function, isFunction: true, useLabel: useLabel)
			   + " "
			   + Application.parenthesize(argument, isFunction: false, useLabel: useLabel)
	}
//
	static private func parenthesize(_ expression: Expressible, isFunction: Bool, useLabel: Bool) -> String
	{
		let needsParens = expression.isMultipart(useLabel: useLabel)
							&& (isFunction ? expression is ConcreteAbstraction
										   : !(expression is Variable))
		return (needsParens ? "(" : "") + expression.stringify(useLabel: true) + (needsParens ? ")" : "")
	}
//
	public func isEqual(to rhs: Expressible) -> Bool
	{
		guard let rhs = rhs as? Application else { return false }
		return self.function.isEqual(to: rhs.function) && self.argument.isEqual(to: rhs.argument)
	}

	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		guard let rhs = rhs as? Application else { return false }
		return self.function.isAlphaEquivalent(to: rhs.function, bindingEquivalences: bindingEquivalences)
			&& self.argument.isAlphaEquivalent(to: rhs.argument, bindingEquivalences: bindingEquivalences)
	}

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		let newFunction = self.function.alphaReduced(substituting: old, with: new)
		let newArgument = self.argument.alphaReduced(substituting: old, with: new)
		if newFunction === function && newArgument === argument
		{
			return self
		}
		else
		{
			return Application(apply: newFunction, to: newArgument)
		}
	}
//
	public func betaReducedOrNil(context: ReductionContext) -> Expressible?
	{
		guard redexCount > 0 else { return nil }
		if let f = function.asAbstraction
		{
			// This is a redex so apply the function to the argument
			return f.substituting(argument: argument, context: context)
		}
		else if let newFunction = function.betaReducedOrNil(context: context)
		{
			return Application(apply: newFunction, to: argument)
		}
		else if let newArgument = argument.betaReducedOrNil(context: context)
		{
			return Application(apply: function, to: newArgument)
		}
		else
		{
			return nil
		}
	}

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		let newFunction = function.substituting(parameter: parameter, with: expression, context: context)
		let newArgument = argument.substituting(parameter: parameter, with: expression, context: context)
		if let newFunction = newFunction
		{
			return Application(apply: newFunction, to: newArgument ?? argument)
		}
		else if let newArgument = newArgument
		{
			return Application(apply: function, to: newArgument)
		}
		else
		{
			return nil
		}
	}

	public var freeVariableNames: Set<String>
	{
		argument.freeVariableNames.union(function.freeVariableNames)
	}

	private enum CodingKeys: String, CodingKey
	{
		case apply
		case redex
		case redexCount
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		let key = function.asAbstraction != nil ? CodingKeys.redex : CodingKeys.apply
		try container.encode([Expression.CodingWrapper(function),
							  Expression.CodingWrapper(argument)], forKey: key)
		try container.encode(redexCount, forKey: .redexCount)
	}

	public func count(freeVariable: FreeVariable) -> Int
	{
		function.count(freeVariable: freeVariable) + argument.count(freeVariable: freeVariable)
	}

	public var asAbstraction: Abstraction? { nil }
}

/// Helper functions and other concrete types for expressions
public struct Expression
{
	/// A wrapper to wrap `Expressible object for encoding.`
	///
	/// This is needed because you can't use protocol instances directly with
	/// an encoder.
	///
	public struct CodingWrapper: Encodable
	{
		let wrapped: Expressible

		init(_ wrapped: Expressible)
		{
			self.wrapped = wrapped
		}

		public func encode(to encoder: Encoder) throws
		{
			try wrapped.encode(to: encoder)
		}
	}
}
