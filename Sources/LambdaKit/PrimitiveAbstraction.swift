//
//  PrimitiveAbstraction.swift
//  
//
//  Created by Jeremy Pereira on 16/04/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox

/// An abstraction that tries to use a built in function for beta reduction
public class PrimitiveAbstraction: Abstraction
{
	private let builtIn: BuiltIn
	private let alternate: Abstraction
	/// The name of the built in
	public let name: String

	/// Initialise a primitive abstraction
	///
	/// A primitive abstraction has a built in function that it attempts to
	/// use for application. If that fails, it uses the alternate.
	/// - Parameters:
	///   - builtIn: Built in function to use
	///   - alternate: Abstraction to use in place of the built in function
	public init(name: String, builtIn: @escaping BuiltIn, alternate: Abstraction)
	{
		self.name = name
		self.builtIn = builtIn
		self.alternate = alternate
		self.redexCount = alternate.redexCount
	}

	public var redexCount: Int

	// MARK: Abstraction

	public func substituting(argument: Expressible, context: ReductionContext) -> Expressible
	{
		builtIn(argument) ?? alternate.substituting(argument: argument, context: context)
	}

	// MARK: Expressible
	public func isEqual(to: Expressible) -> Bool
	{
		self === to
	}

	public var freeVariableNames: Set<String>
	{
		alternate.freeVariableNames
	}

	public func count(freeVariable: FreeVariable) -> Int
	{
		alternate.count(freeVariable: freeVariable)
	}

	// MARK: Stringifiable
	public func stringify(useLabel: Bool) -> String
	{
		useLabel ? label ?? nonLabelString(useLabel: true) : nonLabelString(useLabel: false)
	}

	public func isMultipart(useLabel: Bool) -> Bool
	{
		false
	}

	public var label: String?

	private func nonLabelString(useLabel: Bool) -> String
	{
		"@primitive(\(name) \(alternate.stringify(useLabel: useLabel)))"
	}

	// MARK: BetaReducible

	public func betaReducedOrNil(context: ReductionContext) -> Expressible?
	{
		guard redexCount > 0 else { return nil }
		// If beta reduction would change the alternate we must return a new primitive.
		// If not, we do nothing
		guard let newAlt = alternate.betaReducedOrNil(context: context) else { return nil }
		// The new alt does the same thing as the old alt so we just make a new
		// primitive with the new alt
		return PrimitiveAbstraction(name: name, builtIn: builtIn, alternate: newAlt.asAbstraction!)
	}

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		// We have to assume that substitutions do not chang the functionality
		// of the alternate (this relies on it being correctly defined)
		guard let newAlt = alternate.substituting(parameter: parameter, with: expression, context: context)
		else { return nil }
		return PrimitiveAbstraction(name: name, builtIn: builtIn, alternate: newAlt.asAbstraction!)
	}

	// MARK: AlphaReducible

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		PrimitiveAbstraction(name: name,
							 builtIn: builtIn,
							 alternate: alternate.alphaReduced(substituting: old, with: new).asAbstraction!)
	}

	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		alternate.isAlphaEquivalent(to: rhs, bindingEquivalences: bindingEquivalences)
	}

	public var asAbstraction: Abstraction? { self }

	// MARK: Encodable

	private enum CodingKeys: String, CodingKey
	{
		case name
		case alternate
		case redexCount
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(name, forKey: .name)
		try container.encode(redexCount, forKey: .redexCount)
		try container.encode(Expression.CodingWrapper(alternate), forKey: .alternate)
	}

}
