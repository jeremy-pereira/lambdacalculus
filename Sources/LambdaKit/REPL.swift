//
//  REPL.swift
//  
//
//  Created by Jeremy Pereira on 30/05/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CLKit
import Toolbox
import SystemPackage

private let log = Logger.getLogger("LambdaKit.REPL")

/// A Read Execute Process Loop
///
/// At this time this reads commands which may be either
///
/// - Lambda expressions
/// - let definition
/// - meta commands
/// - An empty line
///
/// The REPL maintains a *current expresion* and a list of let definitions
///
/// - If a lambda expression is entered, it is parsed and the current expression
///   is replaced by it.
/// - If a let definition is read, it is added to the list of let definitions.
///   The current expression is cleared.
/// - If a meta command is read, many different things can happen depending on
///   the command.
/// - If an empty line is read and there is a current expression, one beta
///   reduction is performed on the current expression.
///
public struct REPL
{

	/// Meta state controlling the REPL
	public struct MetaState: OptionSet
	{
		public var rawValue: UInt

		public init(rawValue: UInt)
		{
			self.rawValue = rawValue
		}
		/// Is continuous mode enabled
		///
		/// If continuous mode is enabled, when an expression is entered, it will
		/// be repeatedly beta reduced until it is in normal form. If it is not
		/// enabled, one beta reduction is performed each time you press the return
		/// key. This is a "single step" mode.
		public static let continuous  = MetaState(rawValue: 0b0001)
		/// Are numerals enabled
		///
		/// If numerals are enabled positive whole numbers (and zero) are parsed
		/// as Church numerals. Also, beta reduced expressions are scanned for
		/// Church numerals and replaced with the number they represent. If
		/// numerals are not enabled numbers are just parsed as atoms with no
		/// special meaning.
		public static let numerals    = MetaState(rawValue: 0b0010)

		/// Are annotations enabled
		///
		/// Annotations may be applied to expressions to provide special
		/// processing. For example the `@primitive` annotation allows the
		/// parser to hook in special native functions e.g. finding the
		/// successor or predecessor of a numeral.
		public static let annotations = MetaState(rawValue: 0b0100)

		static let stringRepresentations: [String : MetaState] =
		[
			 "continuous" : .continuous,
			   "numerals" : .numerals,
			"annotations" : .annotations,
		]

		/// The available meta state flags
		public static let availableFlags = Array(stringRepresentations.keys)

		/// Enable flags described by the strings
		/// - Parameter stringRepresentations: list of flag names
		/// - Throws: If any of the flags aren't proper flags
		public mutating func enable<S: Sequence>(stringRepresentations: S) throws
		where S.Element: StringProtocol
		{
			let mentionedFlags: [MetaState] = try stringRepresentations.map
			{
				guard let flag = MetaState.stringRepresentations[String($0)]
				else { throw  Error.unsupportedStateFlag(String($0)) }
				return flag
			}
			self.formUnion(MetaState(mentionedFlags))
		}
		/// Disable flags described by the strings
		/// - Parameter stringRepresentations: list of flag names
		/// - Throws: If any of the flags aren't proper flags
		public mutating func disable<S: Sequence>(stringRepresentations: S) throws
		where S.Element: StringProtocol
		{
			let mentionedFlags: [MetaState] = try stringRepresentations.map
			{
				guard let flag = MetaState.stringRepresentations[String($0)]
				else { throw  Error.unsupportedStateFlag(String($0)) }
				return flag
			}
			self.subtract(MetaState(mentionedFlags))
		}
	}

	/// Errors that may be thrown by the REPL
	public enum Error: Swift.Error
	{
		case letNotSupported
		case emptyMetaCommand
		case unknownMetaCommand(String)
		case missingMetaCommandParameter(String)
		case unsupportedStateFlag(String)
		case cannotLocate(path: String, from: String)
	}

	private struct WrappedOutput: TextOutputStream
	{
		init<T: TextOutputStream>(output: T)
		{
			var varOut = output
			self.writeOutput = { string in varOut.write(string) }
		}
		var writeOutput: (String) -> ()

		mutating func write(_ string: String)
		{
			writeOutput(string)
		}
	}

	/// The meta state of this REPL
	public private(set) var metaState = MetaState()

	private var metaStateStack = Stack<MetaState>()

	/// If true run in continuous mode
	///
	/// If this is true, when an expression is entered, it will be beta
	/// reduced until there are none left to do
	public var continuousMode: Bool
	{
		get { metaState.contains(.continuous) }
		set
		{
			if newValue
			{
				metaState.insert(.continuous)
			}
			else
			{
				metaState.subtract(.continuous)
			}
		}
	}

	/// Enable numeral parsing
	///
	/// If true allows the parser to parse numerals as ``Numeral`` otherwise
	/// numerals are treated as ordinary variables.
	public var enableNumerals: Bool
	{
		get { metaState.contains(.numerals) }
		set
		{
			if newValue
			{
				metaState.insert(.numerals)
			}
			else
			{
				metaState.subtract(.numerals)
			}
		}
	}

	/// Enable annotation parsing
	///
	/// Allows the parser to parse annotations of the form `@something...`. The
	/// only one we have so far is the primitive one.
	public var enableAnnotations: Bool
	{
		get { metaState.contains(.annotations) }
		set
		{
			if newValue
			{
				metaState.insert(.annotations)
			}
			else
			{
				metaState.subtract(.annotations)
			}
		}
	}

	private var output: WrappedOutput
	private var error: WrappedOutput
	private var letList: [(Parameter, Expressible)] = []
	private var expression: Expressible? = nil

	/// Initialise the REPL
	///
	/// The error and output streams are set to the same stream.
	/// - Parameters:
	///   - output: The stream on which to output normal output
	public init<OUT: TextOutputStream>(output: OUT)
	{
		self.init(output: output, error: output)
	}
	/// Initialise the REPL
	///
	/// Allows you to specify a different stream for the output and the errors.
	/// - Parameters:
	///   - output: The stream on which to output normal output
	///   - error: The stream on which to output out of band messages like errors.
	public init<OUT: TextOutputStream, ERR: TextOutputStream>(output: OUT, error: ERR)
	{
		self.output = WrappedOutput(output: output)
		self.error = WrappedOutput(output: error)
	}

	/// Process a line of input.
	/// - Parameter line: The line to process
	/// - Throws: If there is a problem with parsing or executing the line
	public mutating func processInput(line: String) throws
	{
		let trimmedLine = line.trimmingCharacters(in: .whitespaces)
		if trimmedLine.starts(with: "/")
		{
			try processInputAsMeta(line: trimmedLine)
		}
		else if trimmedLine.isEmpty
		{
			if let oldExpression = expression
			{
				let newExpression = oldExpression.betaReducedOrNil(context: ReductionContext(collapseNumerals: self.enableNumerals))
									?? oldExpression
				print(": " + newExpression.stringify(useLabel: true), to: &output)
				expression = newExpression
			}
		}
		else if !trimmedLine.starts(with: "#")
		{
			try processInputAsAST(line: trimmedLine)
		}
	}

	private mutating func processInputAsMeta(line: String) throws
	{
		let commandComponents = line.split(separator: " ", omittingEmptySubsequences: true)
		guard let command = commandComponents.first
		else { throw Error.emptyMetaCommand }

		if command  == "/listDefs"
		{
			let thingToPrint = letList.map
			{
				let (parameter, expression) = $0
				return "let \(parameter.name) = \(expression.stringify(useLabel: false))"
			}.joined(separator: "\n")
			print(": \(letList.count) defnitions:\n" + thingToPrint, to: &output)
		}
		else if command == "/read"
		{
			guard commandComponents.count > 1
			else { throw Error.missingMetaCommandParameter(String(command)) }
			let filePath = commandComponents[1]
			try read(path: String(filePath))
		}
		else if command == "/dump"
		{
			if let expression = self.expression
			{
				let encoder = JSONEncoder()
				encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
				let data = try encoder.encode(Expression.CodingWrapper(expression))
				let encodedString = String(bytes: data, encoding: .utf8)!
				print(encodedString, to: &output)
			}
		}
		else if command == "/toggleContinuous"
		{
			continuousMode = !continuousMode
			print(": Continuous mode is \(continuousMode ? "on" : "off")", to: &output)
		}
		else if command == "/toggleNumerals"
		{
			enableNumerals = !enableNumerals
			print(": Numerals are \(enableNumerals ? "on" : "off")", to: &output)
		}
		else if command == "/toggleAnnotations"
		{
			enableAnnotations = !enableAnnotations
			print(": Annotations are \(enableAnnotations ? "on" : "off")", to: &output)
		}
		else if command == "/enable"
		{
			try metaState.enable(stringRepresentations: commandComponents.dropFirst())
			printMetaState()
		}
		else if command == "/disable"
		{
			try metaState.disable(stringRepresentations: commandComponents.dropFirst())
			printMetaState()
		}
		else if command == "/pushMeta"
		{
			metaStateStack.push(metaState)
			print(": Saved metastate", to: &output)
		}
		else if command == "/popMeta"
		{
			if let retrievedState = metaStateStack.pop()
			{
				metaState = retrievedState
				printMetaState()
			}
			else
			{
				print(": No saved metastate", to: &output)
			}
		}
		else
		{
			throw Error.unknownMetaCommand(String(command))
		}
	}

	private mutating func printMetaState()
	{
		let kvPairs = MetaState.stringRepresentations.sorted
		{
			$0.key < $1.key
		}
		for (name, flag) in kvPairs
		{
			let onOff = metaState.contains(flag) ? "on" : "off"
			print(": \(name) is \(onOff)", to: &output)
		}
	}

	private mutating func processInputAsAST(line: String) throws
	{
		var bindings: [String : Parameter] = [:]
		for (parameter, _) in letList
		{
			bindings[parameter.name] = parameter
		}
		let ast = try Parser(enableNumerals: enableNumerals,
							 enableAnnotations: enableAnnotations)
						.parse(sequence: line, externalBindings: bindings)
		let thingToPrint: String
		switch ast
		{
		case .let(variable: let parameter, argument: var letExpression):
			letExpression.label = parameter.name
			letList.append((parameter, letExpression))
			thingToPrint = "defined \(parameter.name) as \(letExpression.stringify(useLabel: false))"
						 + (expression != nil ? "\n  cleared current expression" : "")
			expression = nil
		case .expression(let newExpression):
			var oldExpression = performExternalReductions(newExpression, definitions: letList)
			if continuousMode
			{
				let start = DispatchTime.now()
				var reductionCount = 0

				try Signal.int.monitor
				{
					let context = ReductionContext(collapseNumerals: self.enableNumerals)
					var newExpression = oldExpression.betaReducedOrNil(context: context) ?? oldExpression
					while newExpression != oldExpression && !$0.wasRaised()
					{
						reductionCount += 1
						oldExpression = newExpression
						newExpression = oldExpression.betaReducedOrNil(context: context) ?? oldExpression
					}
				}
				let end = DispatchTime.now()
				let seconds = Double(end.uptimeNanoseconds - start.uptimeNanoseconds) / 1e9
				print("\(reductionCount) reductions in \(seconds) = \(Double(reductionCount) / seconds) reductions per second", to: &output)
			}
			expression = oldExpression
			thingToPrint = expression?.stringify(useLabel: true) ?? ""
		}
		print(": " + thingToPrint, to: &output)
	}

	/// All the stuff we have made let statements for needs to be substituted in
	///
	/// A `let` is like this: `let p = d` is equvalent to `(λp.e) d` where `e` is everything in the input
	/// after the `let` statement. When we parse expressions, we put bindings in for all the `let` parameters
	/// and now we need to do the reductions. That requires us to build a chain of applications of each lets
	/// argument to an abstraction of our expression, starting with the newest.
	/// If we have a list of definitions `[(a, a b), (c, λx.y)]` and e is our expression, we need to
	/// create `(λc.e) (λx.y)` and then reduce it giving say `f`, then create `(λa.f) (a b)` and
	/// return what it reduces to.
	/// - Parameters:
	///   - expression: The expression to substitute the let definitions into
	///   - definitions: The let definition
	private func performExternalReductions(_ expression: Expressible, definitions: [(Parameter, Expressible)]) -> Expressible
	{
		let fullExpression = definitions.reversed().reduce(expression)
		{
			(e, letDefinition) -> Expressible in
			let (parameter, argument) = letDefinition
			let abstraction = ConcreteAbstraction.create(parameter: parameter, in: e, context: ReductionContext(collapseNumerals: enableNumerals))
			let application = Application(apply: abstraction, to: argument)
			return application
		}
		let ret = definitions.reduce(fullExpression)
		{
			expression, _ in
			expression.betaReducedOrNil(context: ReductionContext(collapseNumerals: self.enableNumerals)) ?? expression
		}
		return ret
	}

	private mutating func read(path: String) throws
	{
		let fm = FileManager.default
		let savedCwd = fm.currentDirectoryPath
		defer { fm.changeCurrentDirectoryPath(savedCwd) }
		log.debug("Reading path '\(path)' CWD '\(savedCwd)'")

		let filePath = FilePath(path)
		let dirName = filePath.removingLastComponent()
		guard let baseName = filePath.lastComponent
		else { throw Error.cannotLocate(path: path, from: savedCwd) }
		if !dirName.isEmpty
		{
			fm.changeCurrentDirectoryPath(dirName.string)
		}

		let url = URL(fileURLWithPath: baseName.string)
		log.debug("URL '\(url)'")

		let fileData = try String(contentsOf: url, encoding: .utf8)
		let lines = fileData.split(separator: "\n")
		for line in lines
		{
			try processInput(line: String(line))
		}
	}
}

