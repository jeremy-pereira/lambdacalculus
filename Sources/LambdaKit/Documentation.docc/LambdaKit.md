# ``LambdaKit``

This is a library that supports the Lambda Calculus. It provides an engine for computation of Lambda expressions and a parser so that expressions in a textual form can be converted into expressions compatible with the computation engine.

## Overview

For an overview on what the Lambda Calculus is and how it works, see my blog posts on the [Lambda Calculus](https://sincereflattery.blog/category/programming/lambda-calculus/).

There are three types of Lambda expression

- *variables*
- *abstractions* of the form `λx.M` where x is a variable and M is a lambda expression
- *applications* of the form `M N` where M and N are both Lambda expressions.

Computation is done simply by looking for applications where the left hand term 
is an abstraction - `(λx.x y) z` would be an example. An application of this 
form is called a *redex*. Once a redex has been found, it is transformed by a 
mechanism known as *beta reduction*. Beta reduction is the process of taking the
body of the abstraction and replacing instances of its parameter (the variable 
following the λ) with the second term of the redex. Then the result replaces the
whole redex. With just this one rule, the Lambda Calculus can be shown to be 
Turing complete.

The easiest way to use this library is to create an instance of a ``REPL`` and 
feed it lambda expressions. Alternatively, you can use the ``Parser`` to parse 
an expression in a textual form into an AST and then run the beta reductions on 
it yourself.

### Licence

All the original code in this package is licensed under the Apache License, 
Version 2.0. You may obtain a copy of the License at 
[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Topics

### Articles

- <doc:WorldsSmallest>
- <doc:Comuptation>
- <doc:HigherLeveLConcepts>
- <doc:Lists>
- <doc:Recursion>
- <doc:Numbers>
