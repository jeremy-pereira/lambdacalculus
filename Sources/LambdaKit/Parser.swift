//
//  Parser.swift
//  
//
//  Created by Jeremy Pereira on 17/02/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// Models an abstract syntax tree
///
/// The abstract syntax tree is either a lambda expression or it's alet expression
/// lets are not part of the Lambda Calculus, so I treat them separately.
public enum AST
{
	case `let`(variable: Parameter, argument: Expressible)
	case expression(Expressible)
}

/// A parser for a lambda expression.
///
/// Lambda expressions have a  fairly simple syntax.
///
/// *statement* ::= let-expression | expression
///
/// *let-expression* ::= "let" variable "=" expression
///
/// *expression* ::= ( variable  |  "(" *expression* ")" | *abstraction* | *application* ) eol
///
/// *abstraction* ::= ("\\" | "λ") variable  "." *expression*
///
/// *application* ::=  *expression* *expression*
///
/// This is an LL(1) parser, so we need to rearrange the grammer slightly.
/// The definition of an application is left recursive which means we don't
/// know how far to descend to get to the bottom of it.
///
/// We define a new term called an *application-list* which is a list of anything
/// except applications. The AST we build is determined by the length of the
/// application list. If it's 1, we just return what got built. If it's longer
/// we return a left recursive series of applications.
///
/// The modified grammar is:
///
/// *statement* ::= let-expression | expression
///
/// *let-expression* ::= `let` variable `=` expression
///
/// *expression* ::= *application-list* eol
///
/// *application-list* ::= *atom**
///
/// If annotations are not enabled:
///
/// *atom* ::= variable | *abstraction* | lpar *application-list* rpar
///
/// *abstraction* ::= lambda variable  dot *application-list*
///
/// Variables may additionally be recognised as numerals, if numerals are enabled
///
/// If annotations are enabled, the definitions are modified as folows
///
/// *atom* ::= variable | *annotation* | *abstraction* | lpar *application-list* rpar
///
/// *annotation* ::= `@primitive` lpar *variable* expression rpar
public struct Parser
{
	fileprivate struct Context
	{
		var currentTok: Token
		var tokens: TokenSequence
		var bindings: [String : Parameter]

		init(tokens: TokenSequence, bindings: [String : Parameter] = [:])
		{
			var myTokens = tokens
			guard let currentTok = myTokens.next() else { fatalError("Should have at least an eol") }
			self.tokens = myTokens
			self.currentTok = currentTok
			self.bindings = bindings
		}

		mutating func getNextToken()
		{
			guard let newTok = tokens.next() else { fatalError("Should have at least an eol") }
			currentTok = newTok
		}
	}

	/// Controls parsing of numerals
	///
	//	 / If true, numbers will be parsed as numerals instead of free variables
	public var enableNumerals: Bool

	/// Controls parsing of annotations
	///
	/// If true, annotation parsing is enabled.
	public var enableAnnotations: Bool

	/// Create a parser
	/// - Parameters:
	///   - enableNumerals: if true, the parser will enable numeral
	///                             parsing. See ``enableNumerals``
	///   - enableAnnotations: if true annotations are enabled
	///
	public init(enableNumerals: Bool = false, enableAnnotations: Bool = false)
	{
		self.enableNumerals = enableNumerals
		self.enableAnnotations = enableAnnotations
	}

	/// Parse a sequence of characters into a lambda expression
	///
	/// - Parameter string: Sequence of characters to parse
	/// - Returns: A result that is either an expression or the error that
	///            stopped the expression from  being produced.
	public func parse<S: Sequence>(sequence: S, externalBindings: [String : Parameter] = [:]) throws -> AST
	where S.Element == Character
	{
		let tokens = TokenSequence(sequence)
		var context = Context(tokens: tokens, bindings: externalBindings)
		return try parseStatement(&context)
	}

	/// Parse a statement
	///
	/// *statement* ::= let-expression | expression
	/// - Parameter context: Context including token list and any bindings
	/// - Throws: If there is a parsing error
	/// - Returns: An AST, either an expression or a let
	private func parseStatement(_ context: inout Context) throws -> AST
	{
		switch context.currentTok
		{
		case .let:
			context.getNextToken()
			return try parseLet(&context)
		default:
			return try .expression(parseExpression(&context))
		}
	}



	/// Parse a let expression
	///
	/// *let-expression* ::= "let" variable "=" expression
	///
	/// The initial `let` is assumed to have been consumed already.
	/// - Parameter context: Context including bindings and  tokeniser
	/// - Throws: If there is a parser error
	/// - Returns: An `AST.let(...)`
	private func parseLet(_ context: inout Context) throws -> AST
	{
		guard case Token.variable(let parameterString) = context.currentTok
		else { throw Error.letWithoutName }
		context.getNextToken()
		guard case Token.equals = context.currentTok
		else { throw Error.letWithoutEquals }
		context.getNextToken()
		let expression = try parseExpression(&context)
		return .let(variable: Parameter(parameterString), argument: expression)
	}

	/// Parse an expression
	///
	/// *expression* ::= *application-list* eol
	///
	/// - Parameter tokens: Tokens to convert into a syntax tree
	/// - Returns: A result containing the parsed tree or an error if the tree
	///            could not b parsed.
	private func parseExpression(_ context: inout Context) throws -> Expressible
	{
		return try parseApplicationList(&context)
	}

	/// Parse an application-list
	///
	/// *application-list* ::= *atom**
	///
	/// - Parameters:
	///   - currentTok: The current first unconsumed token
	///   - tokens: The rest of the tokens
	/// - Returns: A parsed tree
	private func parseApplicationList(_ context: inout Context) throws -> Expressible
	{
		var atoms: [Expressible] = []
		// Parse all the atoms in the application list
		while context.currentTok.isAtomStart
		{
			let atom = try parseAtom(&context)
			atoms.append(atom)
		}
		guard atoms.count != 0 else { throw  Error.emptyExpression }
		// Build an AST of the sequence of applications
		var tree: Expressible? = nil
		for atom in atoms
		{
			if let leftTree = tree
			{
				tree = Application(apply: leftTree, to: atom)
			}
			else
			{
				tree = atom
			}
		}
		return tree!
	}

	private func parseAtom(_ context: inout Context) throws -> Expressible
	{
		switch context.currentTok
		{
		case .variable(let name):
			context.getNextToken()
			if let binding = context.bindings[name]
			{
				return BoundVariable(binding)
			}
			else if name == "@primitive" && enableAnnotations
			{
				return try parsePrimitive(&context)
			}
			else if let number = Int(name), number >= 0 && enableNumerals
			{
				return Numeral(value: number)
			}
			else
			{
				return FreeVariable(name)
			}
		case .lambda:
			context.getNextToken()
			return try parseLambda(&context)
		case .lpar:
			context.getNextToken()
			let expression = try parseApplicationList(&context)
			guard context.currentTok == .rpar else { throw Error.unmatchedParenthesis }
			context.getNextToken()
			return expression
		default:
			throw Error.notImplementedYet("I can't parse anything but variables and lambdas")
		}
	}

	private func parseLambda(_ context: inout Context) throws -> Expressible
	{
		// We will be adding a new binding to the bindings but we want it to go
		// away when we've finished parsing the lambda.
		let savedBindings = context.bindings
		defer { context.bindings = savedBindings }

		let parameter: Parameter
		switch context.currentTok
		{
		case .variable(let name):
			parameter = Parameter(name)
			context.bindings[name] = parameter
		default:
			throw Error.lambdaWithoutBinding
		}
		context.getNextToken()
		guard context.currentTok == .dot else { throw Error.lambdaWithoutDot }
		context.getNextToken()
		let expression = try parseApplicationList(&context)
		return ConcreteAbstraction.create(parameter: parameter,
										  in: expression,
										  context: ReductionContext(collapseNumerals: false))
	}

	/// Parse a primitive
	///
	/// Syntax for a primitive is
	///
	/// *annotation* ::= `@primitive` lpar *variable* expression rpar
	///
	/// We've already parsed the `@primitive` at this point so the current token
	/// is expected to be an lpar
	/// - Parameter context: Parsing context
	/// - Returns: The primitive parsed
	private func parsePrimitive(_ context: inout Context) throws -> Expressible
	{
		guard context.currentTok == Token.lpar
		else { throw Error.expectedLeftParenthesis }
		context.getNextToken()
		guard case Token.variable(let name) = context.currentTok
		else { throw Error.missingBuiltInName }
		context.getNextToken()
		let expression = try parseExpression(&context)
		guard let alternate = expression.asAbstraction
		else  { throw Error.expectedAbstraction }
		guard context.currentTok == Token.rpar
		else { throw Error.unmatchedParenthesis }
		context.getNextToken()
		let builtIn = BuiltInManager.default[name]
		return PrimitiveAbstraction(name: name, builtIn: builtIn, alternate: alternate)
	}
}

extension Parser
{
	public enum Error: Swift.Error
	{
		case invalidToken(String)
		case leftOverTokens(String)
		case notImplementedYet(String)
		case emptyExpression
		case lambdaWithoutBinding
		case lambdaWithoutDot
		case unmatchedParenthesis
		case letWithoutName
		case letWithoutEquals
		case expectedLeftParenthesis
		case missingBuiltInName
		case expectedAbstraction
	}
}

fileprivate extension Parser.Token
{
	var isAtomStart: Bool
	{
		switch self
		{
		case .variable, .lambda, .lpar: return true
		default: return false
		}
	}
}
