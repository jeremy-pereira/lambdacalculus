//
//  BuiltIn.swift
//  
//
//  Created by Jeremy Pereira on 16/04/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// Defines what a BuiltIn looks like
///
/// Returns an optional so we canchoose not to deal with a particular type
/// of expression e.g. a successor function can't deal with non numerals
public typealias BuiltIn = (Expressible) -> Expressible?

/// Mananges built in abstractions
///
/// A built in abstraction is one that is implemented directly in Swift. For
/// example, there might be a successor function defined as ` λn.λf.λx.n f (f x)`
/// which finds the successor of a numeral. This takes three reductions to get a
/// numeral's successort. We can define a Swift function that
/// for any ``Numeral`` return the next `Numeral`.
struct BuiltInManager
{

	private var builtIns: [String : BuiltIn] =
	[
		"numeralSuccessor" : numeralSuccessor,
		"numeralPredecessor" : numeralPredecessor,
	]

	/// Default builtin manager
	static let `default` = BuiltInManager()

	subscript(name: String) -> BuiltIn
	{
		builtIns[name] ?? BuiltInManager.nullFunc
	}

	// MARK: Built in functions

	/// Used as the default
	///
	/// Doesn't work for any expression so always return `nil`
	/// - Parameter expression: An expression that will be ignored
	/// - Returns: `nil` always
	private static func nullFunc(_ expression: Expressible) -> Expressible? { nil }

	/// Numeric successor function
	/// - Parameter expression: Expression to evaluate
	/// - Returns: Successor or `nil`if the argument is not a ``Numeral``
	private static func numeralSuccessor(_ expression: Expressible) -> Expressible?
	{
		guard let numeral = expression as? Numeral else { return nil }

		return numeral.successor
	}
	/// Numeric predecessor function
	/// - Parameter expression: Expression to evaluate
	/// - Returns: Predecessor or `nil`if the argument is not a ``Numeral``
	private static func numeralPredecessor(_ expression: Expressible) -> Expressible?
	{
		guard let numeral = expression as? Numeral else { return nil }

		return numeral.predecessor
	}

}
