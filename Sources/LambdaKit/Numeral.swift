//
//  Numeral.swift
//  
//
//  Created by Jeremy Pereira on 11/04/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// A short cut expression that represents a Church numeral
///
/// A Church numeral is an abstraction that creates the composition of its
/// argument with itself `n` times where `n` is the "value" of the numeral.
/// For example `λf.λx.f x` is `1` and `λf.λx.f (f f (f x)))` is `3`.
public class Numeral: Abstraction
{
	private let value: Int

	public init(value: Int)
	{
		self.value = value
	}

	public let redexCount = 0

	/// The numeral's successor
	public var successor: Numeral { Numeral(value: value + 1) }
	/// The numeral's predecessor - 0 does not have a predecessor
	public var predecessor: Numeral? { value > 0 ? Numeral(value: value - 1) : nil }

	// MARK: - AlphaReducible

	public func alphaReduced(substituting old: Parameter, with new: Parameter) -> Expressible
	{
		// There are no explicit parameters that need substituting
		self
	}

	public func isAlphaEquivalent(to rhs: Expressible, bindingEquivalences: [(Parameter, Parameter)]) -> Bool
	{
		if let rhs = rhs as? Numeral
		{
			return self.value == rhs.value
		}
		else if let rhs = rhs as? ConcreteAbstraction
		{
			return rhs.isAlphaEquivalent(to: longForm, bindingEquivalences: bindingEquivalences)
		}
		else
		{
			return false
		}
	}

	// MARK: BetaReducible

	public func betaReducedOrNil(context: ReductionContext) -> Expressible?
	{
		// A numeral has no redexes in it
		return nil
	}

	public func substituting(parameter: Parameter, with expression: Expressible, context: ReductionContext) -> Expressible?
	{
		// A numeral has no bound variables other than its own
		return nil
	}

	public func betaReduced(binding: (Parameter, Expressible)?, context: ReductionContext) -> Expressible
	{
		// The body of a numeral contains only variables bound in it e.g.
		// 2 = λf.λx.f (f x)  - all variables are bound. There are also no redexes
		// since we only have two nested abstractions and the body of the inner
		// abstraction consists of two nested applications neither of which
		// starts with an abstraction.
		//
		// Thus we always just return self.
		self
	}

	// MARK: Stringifiable

	public var label: String?

	public func isMultipart(useLabel: Bool) -> Bool
	{
		false
	}

	public func stringify(useLabel: Bool) -> String
	{
		useLabel ? label ?? "\(value)" : "\(value)"
	}

	// MARK: Expressible

	public let freeVariableNames = Set<String>()

	public func count(freeVariable: FreeVariable) -> Int
	{
		0
	}

	public func isEqual(to rhs: Expressible) -> Bool
	{
		guard let rhs = rhs as? Numeral else { return false }
		return self.value == rhs.value
	}

	public var asAbstraction: Abstraction? { self }

	// MARK: Abstraction

	public func substituting(argument: Expressible, context: ReductionContext) -> Expressible
	{
		longForm.substituting(argument: argument, context: context)
	}

	/// The numeral written out as a full lambda expression
	///
	/// Todo: Cache the result because it can't be changed
	public var longForm: Abstraction
	{
		let fParam = Parameter("f")
		let xParam = Parameter("x")
		let f = BoundVariable(fParam)
		let x = BoundVariable(xParam)
		var body: Expressible = x
		for _ in 0 ..< value
		{
			body = Application(apply: f, to: body)
		}
		let context = ReductionContext(collapseNumerals: false)
		return ConcreteAbstraction.create(parameter: fParam,
								          in: ConcreteAbstraction.create(parameter: xParam,
																         in: body,
																         context: context),
								          context: context)
	}
}
