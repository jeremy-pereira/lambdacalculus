//
//  Parser+TokenSequence.swift
//  
//
//  Created by Jeremy Pereira on 08/04/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension Parser
{
	enum Token: Equatable
	{
		case lambda
		case variable(String)
		case dot
		case space(String)
		case eol
		case lpar
		case rpar
		case error(Character)
		case `let`
		case equals
	}

	struct TokenSequence: Sequence, IteratorProtocol
	{
		private var characters: AnyIterator<Character>
		private var currentChar: Character?
		private var endSent = false

		init<S: Sequence>(_ characters: S)
		where S.Element == Character
		{
			self.characters = AnyIterator(characters.makeIterator())
			currentChar = self.characters.next()
		}

		private mutating func endToken() -> Token?
		{
			if endSent { return nil }
			else
			{
				endSent = true
				return .eol
			}
		}

		mutating func next() -> Token?
		{
			skipWhiteSpace()
			guard let currentChar = currentChar else { return endToken() }
			if currentChar == "("
			{
				self.currentChar = characters.next()
				return .lpar
			}
			else if currentChar == ")"
			{
				self.currentChar = characters.next()
				return .rpar
			}
			else if currentChar == "."
			{
				self.currentChar = characters.next()
				return .dot
			}
			else if currentChar == "\\" || currentChar == "λ"
			{
				self.currentChar = characters.next()
				return .lambda
			}
			else if currentChar == "="
			{
				self.currentChar = characters.next()
				return .equals
			}
			else if currentChar.isVariableStart
			{
				var variable = String(currentChar)
				self.currentChar = characters.next()
				while let aChar = self.currentChar, !aChar.isVariableTerminator
				{
					variable.append(aChar)
					self.currentChar = characters.next()
				}
				return variable == "let" ? .let : .variable(variable)
			}
			else
			{
				self.currentChar = characters.next()
				return .error(currentChar)
			}
		}

		mutating func skipWhiteSpace()
		{
			while let currentChar = currentChar, currentChar.isWhitespace
			{
				self.currentChar = characters.next()
			}
		}
	}
}

fileprivate extension Character
{
	static let specialCharacters: Set<Character> = ["(", ")", ".", "\\", "λ", "="]

	/// True if `self` can appear at the start of a variable
	var isVariableStart: Bool { !(self.isWhitespace || Character.specialCharacters.contains(self)) }

	/// True if `self` cannot appear in a variable
	///
	/// If parsing a variable and this is `true` it means the last character
	/// was the last character of the variable.
	var isVariableTerminator: Bool
	{
		self.isWhitespace || Character.specialCharacters.contains(self)
	}

	var isLetterExceptLambda: Bool { self.isLetter && self != "λ" }
}
