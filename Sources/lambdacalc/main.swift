//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 27/03/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import ArgumentParser
import Foundation
import LambdaKit
import Toolbox
import LineNoise
import SystemPackage

struct LambdaCalc: ParsableCommand
{
	@Option(help: """
A list of flags to enable in the REPL. These flags affect the way expressions are parsed and reduced. All flags start off disabled, if not enabled by this option.

Available flags are:
\(REPL.MetaState.availableFlags.joined(separator: " ")).
""")
	var enable: String?

	@Option(help: "Specifies a file containing a script of lambdacalc commands and expressions to run")
	var script: String?

	fileprivate struct Session: TextOutputStream
	{
		init<T: TextOutputStream>(output: T)
		{
			var varOut = output
			self.writeOutput = { string in varOut.write(string) }
		}

		var letList: [(Parameter, Expressible)] = []
		var expression: Expressible? = nil

		var writeOutput: (String) -> ()

		mutating func write(_ string: String)
		{
			writeOutput(string)
		}

	}

	func run() throws
	{
		var standardOut = FileHandle.standardOutput
		var standardError = FileHandle.standardError
		let input: FileDescriptor
		if let script = script
		{
			let scriptPath = FilePath(script)
			input = try FileDescriptor.open(scriptPath, .readOnly)
		}
		else
		{
			input = FileDescriptor.standardInput
		}
		defer
		{
			if script != nil
			{
				try! input.close()
			}
		}
		let editor = LineNoise(inputFile: input)
		var carryOn = true
		var repl = REPL(output: standardOut, error: standardError)
		if let enabledFlags = enable
		{
			try repl.processInput(line: "/enable \(enabledFlags)")
		}
		else
		{
			try repl.processInput(line: "/enable")
		}

		while carryOn
		{
			do
			{
				let line = try editor.getLine(prompt: "> ")
				if line.count > 0
				{
					editor.addHistory(line)
				}
				print("", to: &standardOut)
				try repl.processInput(line: line)
			}
			catch LineNoise.Error.EOF
			{
				print("\nBye", to: &standardOut)
				carryOn = false
			}
			catch
			{
				print("\n\(error)", to: &standardError)
			}
		}
	}
}

LambdaCalc.main()
