# LambdaCalculus

This is a Swift package to implement the Lambda Calculus as described in [Wikipedia](https://en.wikipedia.org/wiki/Lambda_calculus). 

This is a standard Swift package, and can be found at:

    https://jeremy-pereira@bitbucket.org/jeremy-pereira/lambdacalculus.git

A discussion of how the Lambda Calculus works can be found in the following sequence of articles: [https://sincereflattery.blog/category/programming/lambda-calculus/](https://sincereflattery.blog/category/programming/lambda-calculus/)

