let cond = (λp.λa.λb.p a b)

let true = (λtrue_x.λtrue_y.true_x)
let false = (λfalse_x.λfalse_y.false_y)

let not = λa.a false true
let and = λa.λb.a b false
let or = λa.λb.a true b
let equals = λa.λb.a b (b false true)

let cons = λcons_h.λcons_t.(λcons_f.cons_f cons_h cons_t)
let tail = λtail_l.tail_l (λtail_x.λtail_y.tail_y)
let head = λhead_l.head_l (λhead_x.λhead_y.head_x)
let isEmpty = λisEmpty_l.isEmpty_l (λisEmpty_h.λisEmpty_t.false)
let nil = λnil_f.true

let l1 =  λl1_f.l1_f banana nil
let l2 =  λl2_f.l2_f pear l1
let l3 = λl3_f.l3_f apple l2

let naiveLast = λnaiveLast_l.(isEmpty (tail naiveLast_l)) (head naiveLast_l) (naiveLast (tail naiveLast_l))

let 0 = nil
let inc = cons 1

let count' = λcount'_func.λcount'_list.(isEmpty count'_list) 0 (inc (count'_func (tail count'_list)))
let countq = λcountq_q.count' (countq_q countq_q)

let Y = λf.(λy.f(y y)) (λy.f(y y))

#reverseInto = λl1.λl2.if (isEmpty l1) then l2 else (reverseInto (tail l1) (cons (head l1) l2))
#reverseInto' = (λf.λl1.λl2.if (isEmpty l1) then l2 else (f (tail l1) (cons (head l1) l2))) reverseInto'
#             = (λf.λl1.λl2.cond (isEmpty l1) l2 (f (tail l1) (cons (head l1) l2))) reverseInto'
             
# let foo = λx.x foo

let foo' = λf.λx.x f
let fooq = λq.foo' (q q)

