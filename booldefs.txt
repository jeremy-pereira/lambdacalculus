let cond = (λp.λa.λb.p a b)

let true = (λx.λy.x)
let false = (λx.λy.y)

let not = λa.a false true
let and = λa.λb.a b false
let or = λa.λb.a true b
let equals = λa.λb.a b (b false true)
