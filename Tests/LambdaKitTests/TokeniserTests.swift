//
//  TokeniserTests.swift
//  
//
//  Created by Jeremy Pereira on 08/04/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import Toolbox
import XCTest
@testable import LambdaKit

private let log = Logger.getLogger("LambdaKitTests.ParserTests")

final class TokeniserTests: XCTestCase
{
	func testSimpleTokenise()
	{
		let string1 = "(λx.x) y"
		let expected1: [Parser.Token] = [
			.lpar, .lambda, .variable("x"), .dot, .variable("x"), .rpar, .variable("y"), .eol
		]
		let tokens = Array(Parser.TokenSequence(string1))
		XCTAssert(tokens.count == expected1.count, "Wrong count, tokens are \(tokens)")
		zip(tokens, expected1).forEach
		{
			XCTAssert($0 == $1, "Expected \($1), got \($0)")
		}

	}

	func testLongVariableName()
	{
		let string1 = "(λxy.xy) abc"
		let expected1: [Parser.Token] = [
			.lpar, .lambda, .variable("xy"), .dot, .variable("xy"), .rpar, .variable("abc"), .eol
		]
		let tokens = Array(Parser.TokenSequence(string1))
		XCTAssert(tokens.count == expected1.count, "Wrong count, tokens are \(tokens)")
		zip(tokens, expected1).forEach
		{
			XCTAssert($0 == $1, "Expected \($1), got \($0)")
		}

	}

	func testMultiWhiteSpace()
	{
		let string1 = "(λxy.xy)\t abc"
		let expected1: [Parser.Token] = [
			.lpar, .lambda, .variable("xy"), .dot, .variable("xy"), .rpar, .variable("abc"), .eol
		]
		let tokens = Array(Parser.TokenSequence(string1))
		XCTAssert(tokens.count == expected1.count, "Wrong count, tokens are \(tokens)")
		zip(tokens, expected1).forEach
		{
			XCTAssert($0 == $1, "Expected \($1), got \($0)")
		}
	}

// Since variables can currently be anything except one of the "keywords",
// this test is no good at the moment.
//	func testErrorTokenise()
//	{
//		let string1 = "(λx.x)% y"
//		let expected1: [Parser.Token] = [
//			.lpar, .lambda, .variable("x"), .dot, .variable("x"), .rpar, .error("%"), .variable("y"), .eol
//		]
//		let tokens = Array(Parser.TokenSequence(string1))
//		XCTAssert(tokens.count == expected1.count, "Wrong count, tokens are \(tokens)")
//		zip(tokens, expected1).forEach
//		{
//			XCTAssert($0 == $1, "Expected \($1), got \($0)")
//		}
//	}

	func testLetAndEquals()
	{
		let string1 = "let x = y"
		let expected1: [Parser.Token] = [
			.let, .variable("x"), .equals, .variable("y"), .eol
		]
		let tokens = Array(Parser.TokenSequence(string1))
		XCTAssert(tokens.count == expected1.count, "Wrong count, tokens are \(tokens)")
		zip(tokens, expected1).forEach
		{
			XCTAssert($0 == $1, "Expected \($1), got \($0)")
		}
	}

	static var allTests = [
		("testDescriptions", testSimpleTokenise),
		("testLongVariableName", testLongVariableName),
		("testMultiWhiteSpace", testMultiWhiteSpace),
	]
}
