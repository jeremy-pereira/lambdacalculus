//
//  ParserTests.swift
//  
//
//  Created by Jeremy Pereira on 17/02/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import XCTest
@testable import LambdaKit

private let log = Logger.getLogger("LambdaKitTests.ParserTests")

final class ParserTests: XCTestCase
{
	func testParseVariable()
	{
		let vString = "x"

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == FreeVariable(vString))
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseOneApplication()
	{
		let vString = "x y"

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
		 	let expectedResult = apply("x", "y")
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseTwoApplications()
	{
		let vString = "x y z"
		let expectedResult = apply(apply("x", "y"), "z")
		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseLambda()
	{
		let vString = "λx.y"
		let expectedResult = lambda("x", "y")

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseParentheses()
	{
		let vString = "x (y z)"
		let expectedResult = apply("x", apply("y", "z"))

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseLambdaApplication()
	{
		let vString = "(λx.y) z"
		let expectedResult = apply(lambda("x", "y"), "z")

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testBoundEquals()
	{
		let string1 = "λx.y x z"
		let string2 = "λx.y x z"

		do
		{
			let result1 = try Parser().parse(sequence: string1).get()
			let result2 = try Parser().parse(sequence: string2).get()
			XCTAssert(result1.isAlphaEquivalent(to: result2), "Failed to get equality, \nr1 '\(result1)', \nr2 '\(result2)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testAlphaEquivalence()
	{
		let string1 = "λx.y x z"
		let string2 = "λa.y a z"
		let string3 = "λb.y b z"

		do
		{
			let result1 = try Parser().parse(sequence: string1).get()
			let result2 = try Parser().parse(sequence: string2).get()
			let result3 = try Parser().parse(sequence: string3).get()
			XCTAssert(result1.isAlphaEquivalent(to: result2), "Failed to get alpha equivalence, \nr1 '\(result1)', \nr2 '\(result2)'")
			XCTAssert(result2.isAlphaEquivalent(to: result1), "Failed to get alpha equivalence, \nr1 '\(result1)', \nr2 '\(result2)'")
			XCTAssert(result2.isAlphaEquivalent(to: result3), "Failed to get alpha equivalence, \nr2 '\(result2)', \nr3 '\(result3)'")
			XCTAssert(result2.isAlphaEquivalent(to: result2), "Failed to get alpha equivalence to self, \nr2 '\(result2)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseEverythingToTheRight()
	{
		let vString = "\\x.a b"
		let expectedResult = lambda("x", apply("a", "b"))

		do
		{
			let expression = try Parser().parse(sequence: vString).get()
			XCTAssert(expression == expectedResult, "Incorrect expression, got \(expression)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testLet()
	{
		let vString = "let x = λx.y"
		let letExpression = lambda("x", "y")

		do
		{
			let result = try Parser().parse(sequence: vString)
			switch result
			{
			case .let(variable: let variable, argument: let argument):
				XCTAssert(variable.name == "x")
				XCTAssert(argument == letExpression)
			case .expression(let expression):
				XCTFail("Got an expression: \(expression)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParseScopedVariables()
	{
		log.pushLevel(.info)
		defer { log.popLevel() }

		let vString = "λx.λx.(λx.x) x"

		do
		{
			let result = try Parser().parse(sequence: vString).get()
			log.info("\(result)")
		}
		catch
		{
			XCTFail("\(error)")
		}

	}

	static var allTests = [
		("testParseVariable", testParseVariable),
		("testParseOneApplication", testParseOneApplication),
		("testParseTwoApplications", testParseTwoApplications),
		("testParseLambda", testParseLambda),
		("testParseParentheses", testParseParentheses),
		("testParseLambdaApplication", testParseLambdaApplication),
		("testParseEverythingToTheRight", testParseEverythingToTheRight),
		("testAlphaEquivalence", testAlphaEquivalence),
	]
}
