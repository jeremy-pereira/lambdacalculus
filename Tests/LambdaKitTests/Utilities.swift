//
//  Utilities.swift
//  
//
//  Created by Jeremy Pereira on 07/03/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import LambdaKit

func lambda(_ variable: String, _ expression: Expressible) -> Expressible
{
	return ConcreteAbstraction.create(parameterName: variable, in: expression)
}

func lambda(_ variable: String, _ expression: String) -> Expressible
{
	return ConcreteAbstraction.create(parameterName: variable, in: FreeVariable(expression))
}

func apply(_ first: Expressible, _ second: Expressible) -> Expressible
{
	return Application(apply: first, to: second)
}

func apply(_ first: String, _ second: String) -> Expressible
{
	return apply(FreeVariable(first), FreeVariable(second))
}

func apply(_ first: Expressible, _ second: String) -> Expressible
{
	return apply(first, FreeVariable(second))
}

func apply(_ first: String, _ second: Expressible) -> Expressible
{
	return apply(FreeVariable(first), second)
}

func `let`(_ variable: String, equals expression: Expressible) -> AST
{
	return .let(variable: Parameter(variable), argument: expression)
}


extension AST
{
	enum Error: Swift.Error
	{
		case notAnExpression
		case notALet
	}

	func get() throws -> Expressible
	{
		switch self
		{
		case .expression(let ret):
			return ret
		case .let:
			throw Error.notAnExpression
		}
	}
}

class TestOutputStream: TextOutputStream
{
	private(set) var lines: [String] = []

	func write(_ string: String)
	{
		lines.append(string)
	}

	func reset()
	{
		lines.removeAll()
	}
}

extension Abstraction
{
	func substituting(argument: Expressible) -> Expressible
	{
		return self.substituting(argument: argument, context: ReductionContext(collapseNumerals: false))
	}
}

extension Expressible
{
	func betaReduced() -> Expressible
	{
		return self.betaReduced(context: ReductionContext(collapseNumerals: false))
	}
}
