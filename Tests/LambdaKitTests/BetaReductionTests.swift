//
//  BetaReductionTests.swift
//  
//
//  Created by Jeremy Pereira on 07/03/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import XCTest
@testable import LambdaKit

private let log = Logger.getLogger("LambdaKitTests.BetaReductionTests")

final class BetaReductionTests: XCTestCase
{
	func testSimpleReduction()
	{
		var string = ""
		string = "(λx.x) y"
		do
		{
			let expression1 = try Parser().parse(sequence: string).get()
			let reduced1 = expression1.betaReduced()
			XCTAssert(reduced1 == FreeVariable("y"), "Expected reduction to y, got \(reduced1)")
		}
		catch
		{
			XCTFail("\(string): \(error)")
		}
		string = "(λx.z) y"
		do
		{
			let expression1 = try Parser().parse(sequence: string).get()
			let reduced1 = expression1.betaReduced()
			XCTAssert(reduced1 == FreeVariable("z"), "Expected reduction to z, got \(reduced1)")
		}
		catch
		{
			XCTFail("\(string): \(error)")
		}
	}

	func testScopingAmbiguity()
	{
		Logger.pushLevel(.debug, forName: "LambdaKit.Expression")
		defer { Logger.popLevel(forName: "LambdaKit.Expression") }
		log.pushLevel(.debug)
		defer { log.popLevel() }

		let string = "((λx.λy.x)y)z"
		do
		{
			let expression1 = try Parser().parse(sequence: string).get()
			let reduced1 = expression1.betaReduced()
			log.debug("Intermediate: \(reduced1)")
			let reduced2 = reduced1.betaReduced()
			XCTAssert(reduced2 == FreeVariable("y"), "Expected reduction to y, got \(reduced2)")
		}
		catch
		{
			XCTFail("\(string): \(error)")
		}
	}

	func testConditional()
	{
		log.pushLevel(.debug)
		defer { log.popLevel() }
		let cond = "(λp.λa.λb.p a b)(λx.λy.x) M N"
		do
		{
			let i1 = try Parser().parse(sequence: "(λa.λb.(λx.λy.x) a b) M N").get()
			let i2 = try Parser().parse(sequence: "(λb.(λx.λy.x) M b) N").get()
			let expression1 = try Parser().parse(sequence: cond).get()
			let reduced1 = expression1.betaReduced()
			XCTAssert(reduced1.isAlphaEquivalent(to: i1), "i1 incorrect, expected '\(i1)', got '\(reduced1)'")
			log.debug("Intermediate: \(reduced1)")
			let reduced2 = reduced1.betaReduced()
			XCTAssert(reduced2.isAlphaEquivalent(to: i2), "i2 incorrect, expected '\(i2)', got '\(reduced2)'")
			log.debug("Second Intermediate: \(reduced2)")
		}
		catch
		{
			XCTFail("\(cond): \(error)")
		}
	}

	/// Test recursive bindings
	///
	/// Through the use of the Y combinator we can have recursive bindings. We
	/// can indirectly substitute a function into itself, but because the
	/// binding is identical i.e. the same object, we must make sure we don't
	/// accidentally substitute the bindings in the inner function with the
	/// one for the outer function. Consider:
	/// ```
	/// #let foo = λx.x foo
	/// let foo' = λf.λx.x f
 	/// let fooq = λq.foo' (q q)
	/// (fooq fooq) apple
	/// ```
	/// The last line should reduce to
	/// ```
	///  (λx.x ((λq.(λf.λx.x f) (q q)) (λq.(λf.λx.x f) (q q)))) apple
	/// ```
	/// and then to
	/// ```
	///  apple ((λq.(λf.λx.x f) (q q)) (λq.(λf.λx.x f) (q q))))
	/// ```
	/// However, on versions up to 1.3.0, it reduces to
	/// ```
	/// apple ((λq.(λf.λx.apple f) (q q)) (λq.(λf.λx.apple f) (q q)))
	/// ```
	/// This is because we substituted the identical function with the identical
	/// binding and when the parameter of the "outer" function was substituted,
	/// the inner one was done too.
	///
	/// I'm using the Y combinator to build the expression
	///
	func testRecursiveBinding()
	{
		do
		{
			log.pushLevel(.debug)
			defer { log.popLevel() }

			let fooDash = try Parser().parse(sequence: "λf.λx.x f").get()
			let Y = try Parser().parse(sequence: "λf.(λy.f(y y)) (λy.f(y y))").get()
			let apple = FreeVariable("apple")
			let theApplication = apply(apply(Y, fooDash), apple)
			// We need to reduce four times to get "apple" substituted in
			let reduced = theApplication.betaReduced().betaReduced().betaReduced().betaReduced()
			log.debug("Reduced expression: '\(reduced)'")
			XCTAssert(reduced.count(freeVariable: apple) == 1, "Invalid apple count in \(reduced.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	static var allTests =
	[
		("testSimpleReduction", testSimpleReduction),
		("testScopingAmbiguity", testScopingAmbiguity),
		("testConditional", testConditional),
	]

}
