//
//  NumeralTests.swift
//  
//
//  Created by Jeremy Pereira on 11/04/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import LambdaKit
import Toolbox

class NumeralTests: XCTestCase
{

	func testDescription()
	{
		let n = Numeral(value: 3)
		XCTAssert(n.description == "3")
	}

	func testStringify()
	{
		let n = Numeral(value: 4)
		XCTAssert(n.stringify(useLabel: true) == "4")
		XCTAssert(n.stringify(useLabel: false) == "4")
		n.label = "5"
		XCTAssert(n.stringify(useLabel: true) == "5")
		XCTAssert(n.stringify(useLabel: false) == "4")
	}

	func testBetaReduction()
	{
		let three = Numeral(value: 3)
		let expression1 = apply(three, "foo")
		let outerExpression = apply(expression1, "apple")
		// Now we have "3 foo apple" which should reduce to foo (foo (foo apple))
		let expected = apply("foo", apply("foo", apply("foo", "apple")))

		let result = outerExpression.betaReduced().betaReduced()
		XCTAssert(expected == result, "expected: \(expected.description), got \(result.description)")
	}

	func testBetaReductionEmbedded()
	{
		let three = Numeral(value: 3)
		let parameter = Parameter("x")
		let x = BoundVariable(parameter)
		let expression1 = apply(three, x)
		let outerExpression = ConcreteAbstraction.create(parameter: parameter, in: expression1)
		let outerApp = apply(outerExpression, "apple")
		let result = outerApp.betaReduced()
		let expected = apply(three, "apple")
		XCTAssert(expected == result)
	}

	func testAlphaEquivalence()
	{
		let two1 = Numeral(value: 2)
		let two2 = Numeral(value: 2)
		XCTAssert(two1 !== two2)
		let three = Numeral(value: 3)
		XCTAssert(two1.isAlphaEquivalent(to: two2))
		XCTAssertFalse(two1.isAlphaEquivalent(to: three))
		XCTAssertFalse(three.isAlphaEquivalent(to: FreeVariable("x")))
		XCTAssertFalse(FreeVariable("apple").isAlphaEquivalent(to: three))
		let string = "λf.λx.f (f (f x))"
		do
		{
			let threeExplicit = try Parser().parse(sequence: string).get()
			XCTAssert(threeExplicit.isAlphaEquivalent(to: three), "Concrete abstraction to numeral alpha reduction is broken")
			XCTAssert(three.isAlphaEquivalent(to: threeExplicit))
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParsing()
	{
		var p = Parser()
		let testString = "3 apple banana"
		do
		{
			let expression1 = try p.parse(sequence: testString).get()
			let result1 = expression1.betaReduced().betaReduced()
			XCTAssert(expression1 == result1)
			p.enableNumerals = true
			let expression2 = try p.parse(sequence: testString).get()
			let result2 = expression2.betaReduced().betaReduced()
			let expected2 = apply("apple", apply("apple", apply("apple", "banana")))
			XCTAssert(expected2 == result2, "Expected \(expected2.description), got \(result2.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testParsingNegative()
	{
		var p = Parser()
		let testString = "-3 apple banana"
		do
		{
			let expression1 = try p.parse(sequence: testString).get()
			let result1 = expression1.betaReduced().betaReduced()
			XCTAssert(expression1 == result1)
			p.enableNumerals = true
			let expression2 = try p.parse(sequence: testString).get()
			let result2 = expression2.betaReduced().betaReduced()
			let expected2 = result1
			XCTAssert(expected2 == result2, "Expected \(expected2.description), got \(result2.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testLC27()
	{
		var p = Parser()
		let testString = "(λn.λf.λx.n f (f x)) 2"	// This is applying the successor function to 2
		let expectt1String = "λf.λx.2 f (f x)"
		let expect2String = "λf.λx.f (f (f x))"
		do
		{
			let expression1 = try p.parse(sequence: testString).get()
			let expect1 = try p.parse(sequence: expectt1String).get()
			let expect2 = try p.parse(sequence: expect2String).get()
			let result1 = expression1.betaReduced().betaReduced()
			XCTAssert(expect1.isAlphaEquivalent(to: result1), "Expected\n\(expect1.description)\nGot\n\(result1.description)")
			p.enableNumerals = true
			let expression2 = try p.parse(sequence: testString).get()
			// Need three reductions to fully reduce succ
			let result2 = expression2.betaReduced().betaReduced().betaReduced()
			XCTAssert(expect2.isAlphaEquivalent(to: result2),
					  "Expected\n\(expect2.description)\n, got\n\(result2.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testDetectNewNumerals()
	{
		var p = Parser()
		p.enableNumerals = true
		let testString = "λf.λx.(λx.x) (f (f x))"	// Beta reducing this should give λf.λx.f (f x) = 2
		do
		{
			let expression = try p.parse(sequence: testString).get()
			let result = expression.betaReduced(context: ReductionContext(collapseNumerals: true))
			XCTAssert(result.description == "2", "expected 2, got\n\(result.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

//	func testReductionContext()
//	{
//		Logger.pushLevel(.debug, forName: "LambdaKit.Expression")
//		defer { Logger.popLevel(forName: "LambdaKit.Expression") }
//		var p = Parser()
//		p.enableNumerals = true
//		let testString = "(λn.λf.λx.n f (f x)) (λf.λx.x)"
//		let expectString = "1"
//		do
//		{
//			let expression = try p.parse(sequence: testString).get()
//			let expect = try p.parse(sequence: expectString).get()
//			let context = ReductionContext(collapseNumerals: true)
//			let result = expression
//							.betaReduced(context: context)
//							.betaReduced(context: context)
//							.betaReduced(context: context)
//			XCTAssert(result is Numeral, "Result should be a numeral. Got \(result.description)")
//			XCTAssert(expect.isAlphaEquivalent(to: result), "Expected\n\(expect.description)\nGot\n\(result.description)")
//		}
//		catch
//		{
//			XCTFail("\(error)")
//		}
//
//	}

	func testEquality()
	{
		let two = Numeral(value: 2)
		let three = Numeral(value: 3)
		XCTAssert(two != three)
		XCTAssert(two == Numeral(value: 2))
	}

	
}
