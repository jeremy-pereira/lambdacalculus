//
//  REPLTests.swift
//  
//
//  Created by Jeremy Pereira on 30/05/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import LambdaKit

final class ReplTests: XCTestCase
{
	func testLet()
	{
		let letString = "let x = x"
		let output = TestOutputStream()
		let expectedOutput = ["", ": defined x as x", "\n"]
		var repl = REPL(output: output)
		do
		{
			try repl.processInput(line: letString)
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testASTAndReduce()
	{
		let expressionString = "(λx.x) y"
		let output = TestOutputStream()
		let expectedOutput = ["", ": (λx.x) y", "\n", "", ": y", "\n"]
		var repl = REPL(output: output)
		do
		{
			try repl.processInput(line: expressionString)
			try repl.processInput(line: "")
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSuperfluousParentheses()
	{
		let letString = "let foo = λx.x"
		let output = TestOutputStream()
		let expectedOutput = ["", ": defined foo as λx.x", "\n", "", ": foo foo", "\n"]
		var repl = REPL(output: output)
		do
		{
			try repl.processInput(line: letString)
			try repl.processInput(line: "foo foo")
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testListDefs()
	{
		let letString = "let x = x"
		let output = TestOutputStream()
		let expectedOutput = ["", ": defined x as x", "\n", "", ": 1 defnitions:\nlet x = x", "\n"]
		var repl = REPL(output: output)
		do
		{
			try repl.processInput(line: letString)
			try repl.processInput(line: "/listDefs")
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testRead()
	{
		let fileName = "testdef.txt"
		let letString = "let x = x"
		let data = Data(letString.utf8)
		let output = TestOutputStream()
		let expectedOutput = ["", ": defined x as x", "\n"]
		var repl = REPL(output: output)
		do
		{
			try data.write(to: URL(fileURLWithPath: fileName))
			try repl.processInput(line: "/read \(fileName)")
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testNumerals()
	{
		let expressionString = "3 apple banana"
		let output = TestOutputStream()
		let expectedOutput = ["", ": \(expressionString)", "\n", "", ": \(expressionString)", "\n"]
		let reducedNumeral = ["", ": \(expressionString)", "\n", "", ": (λx.apple (apple (apple x))) banana", "\n"]
		var repl = REPL(output: output)
		do
		{
			try repl.processInput(line: expressionString)
			try repl.processInput(line: "")
			XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
			repl.enableNumerals = true
			try repl.processInput(line: expressionString)
			try repl.processInput(line: "")
			let expected2 = expectedOutput + reducedNumeral
			XCTAssert(output.lines == expected2, "Wrong lines: Expected \n\(expected2)\nGot\n\(output.lines)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPrimitives() throws
	{
		let testString = "@primitive(numeralSuccessor λn.λf.λx.n f (f x)) 3"
		let expectedOutput = ["", ": \(testString)", "\n", "", ": 4", "\n"]
		let output = TestOutputStream()
		var repl = REPL(output: output)
		repl.enableNumerals = true
		repl.enableAnnotations = true
		try repl.processInput(line: testString)
		try repl.processInput(line: "")
		XCTAssert(output.lines == expectedOutput, "Wrong lines: Expected \n\(expectedOutput)\nGot\n\(output.lines)")

	}

	func testLetPrimitives() throws
	{
		let letString = "let succ = @primitive (numeralSuccessor (λn.λf.λx.n f (f x)))"
		let output = TestOutputStream()
		var repl = REPL(output: output)
		repl.enableAnnotations = true
		repl.enableNumerals = true
		try repl.processInput(line: letString)
		print(output.lines)
		let testString = "succ 3"
		try repl.processInput(line: testString)
		output.reset()
		try repl.processInput(line: "")
		print(output.lines)
		let expectedOutput = ["", ": 4", "\n"]
		XCTAssert(expectedOutput == output.lines)
	}

	func testLetPrimPred() throws
	{
		let letStrings =
		[
			"let true = (λx.λy.x)",
			"let false = (λx.λy.y)",
			"let succ = @primitive(numeralSuccessor λn.λf.λx.n f (f x))",
			"let pair = λa.λb.λp.p a b",
			"let addSuccFirst = λp.pair (succ (p true)) (p true)",
			"let pred = @primitive(numeralPredecessor λn.(n addSuccFirst (λp.p 0 0)) false)"
		]
		let output = TestOutputStream()
		var repl = REPL(output: output)
		repl.enableAnnotations = true
		repl.enableNumerals = true
		for string in letStrings
		{
			try repl.processInput(line: string)
		}
		print(output.lines.joined())
		let testString = "pred 3"
		repl.continuousMode = true
		try repl.processInput(line: testString)
		output.reset()
		try repl.processInput(line: "")
		print(output.lines.joined())
		let expectedOutput = ["", ": 2", "\n"]
		XCTAssert(expectedOutput == output.lines, "expected 2, got:\n\(output.lines.joined())")
	}
//
//	func testDump()
//	{
//		let expressionString = "(λx.x) y"
//		let output = TestOutputStream()
//		let expectedOutput = ["", ": (λx.x) y", "\n", "",
//							  "{\n  \"redex\" : [\n    {\n      \"_lambda\" : \"x\",\n      \"body\" : {\n        \"bound\" : \"x\"\n      },\n      \"redexCount\" : 0\n    },\n    \"y\"\n  ],\n  \"redexCount\" : 1\n}", "\n"]
//		var repl = REPL(output: output)
//		do
//		{
//			try repl.processInput(line: expressionString)
//			try repl.processInput(line: "/dump")
//			XCTAssert(output.lines == expectedOutput, "Wrong lines: expected\n\(expectedOutput)\ngot\n\(output.lines)")
//		}
//		catch
//		{
//			XCTFail("\(error)")
//		}
//	}

	func testEnableFlags() throws
	{
		let output = TestOutputStream()
		var repl = REPL(output: output)
		try repl.processInput(line: "/enable annotations continuous numerals")
		XCTAssert(repl.metaState.contains(.annotations))
		XCTAssert(repl.metaState.contains(.numerals))
		XCTAssert(repl.metaState.contains(.continuous))
		XCTAssert(repl.enableNumerals)
		XCTAssert(repl.enableAnnotations)
		XCTAssert(repl.continuousMode)
		try repl.processInput(line: "/disable annotations numerals")
		XCTAssert(!repl.metaState.contains(.annotations))
		XCTAssert(!repl.metaState.contains(.numerals))
		XCTAssert(repl.metaState.contains(.continuous))
		XCTAssert(!repl.enableNumerals)
		XCTAssert(!repl.enableAnnotations)
		XCTAssert(repl.continuousMode)
		try repl.processInput(line: "/enable annotations continuous numerals")
		repl.enableNumerals = false
		repl.enableAnnotations = false
		repl.continuousMode = false
		XCTAssert(repl.metaState.isEmpty)
		try repl.processInput(line: "/toggleAnnotations")
		XCTAssert(repl.metaState.contains(.annotations))
		try repl.processInput(line: "/toggleNumerals")
		XCTAssert(repl.metaState.contains(.numerals))
		try repl.processInput(line: "/toggleContinuous")
		XCTAssert(repl.metaState.contains(.continuous))
	}

	func testPushPop() throws
	{
		let output = TestOutputStream()
		var repl = REPL(output: output)
		try repl.processInput(line: "/enable annotations continuous numerals")
		output.reset()
		try repl.processInput(line: "/pushMeta")
		print(output.lines.joined())
		XCTAssert(output.lines.joined() == ": Saved metastate\n")
		try repl.processInput(line: "/disable annotations continuous numerals")
		XCTAssert(repl.metaState.isEmpty)
		try repl.processInput(line: "/popMeta")
		XCTAssert(repl.metaState.contains(.annotations))
		XCTAssert(repl.metaState.contains(.numerals))
		XCTAssert(repl.metaState.contains(.continuous))
		output.reset()
		try repl.processInput(line: "/popMeta")
		XCTAssert(output.lines.joined() == ": No saved metastate\n")
	}

	func testRelativeNestedRead() throws
	{
		Logger.pushLevel(.debug, forName: "LambdaKit.REPL")
		defer { Logger.popLevel(forName: "LambdaKit.REPL") }
		guard let bundleUrl = Bundle.module.url(forResource: "top1", withExtension: "txt")
		else
		{
			fatalError("The first read file is missing")
		}
		let output = TestOutputStream()
		var repl = REPL(output: output)
		try repl.processInput(line: "/read \(bundleUrl.path)")
		print(output.lines)
		//XCTAssert(output.lines == expectedOutput, "Wrong lines: \(output.lines)")
	}
}

