//
//  EncoderTests.swift
//  
//
//  Created by Jeremy Pereira on 15/05/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import Toolbox
import XCTest
@testable import LambdaKit

private let log = Logger.getLogger("LambdaKitTests.EncoderTests")

final class EncoderTests: XCTestCase
{
	func testVariable()
	{
		let expression = FreeVariable("apple")
		let encoder = JSONEncoder()
		do
		{
			let string = try String(bytes: encoder.encode(expression), encoding: .utf8)!
			XCTAssert(string == "\"apple\"", "Wrong encoding: '\(string)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testApplication()
	{
		let expression = apply("apple", "banana")
		let expectedResult = #"{"apply":["apple","banana"],"redexCount":0}"#
		let encoder = JSONEncoder()
		encoder.outputFormatting = [.sortedKeys]
		do
		{
			let string = try String(bytes: encoder.encode(Expression.CodingWrapper(expression)),
									encoding: .utf8)!
			XCTAssert(string == expectedResult, "Wrong encoding: '\(string)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testExpressible()
	{
		let expression: Expressible = apply("apple", "banana")
		let expectedResult = #"{"apply":["apple","banana"],"redexCount":0}"#
		let encoder = JSONEncoder()
		encoder.outputFormatting = [.sortedKeys]
		do
		{
			let string = try String(bytes: encoder.encode(Expression.CodingWrapper(expression)),
									encoding: .utf8)!
			XCTAssert(string == expectedResult, "Wrong encoding: '\(string)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

//	func testAbstractionSimpleNoBinding()
//	{
//		let expression = lambda("apple", "banana")
//		let expectedResult = #"{"_lambda":"apple","body":"banana","redexCount":0}"#
//		let encoder = JSONEncoder()
//		encoder.outputFormatting = [.sortedKeys]
//		do
//		{
//			let string = try String(bytes: encoder.encode(Expression.CodingWrapper(expression)),
//									encoding: .utf8)!
//			XCTAssert(string == expectedResult, "Wrong encoding: '\(string)'")
//		}
//		catch
//		{
//			XCTFail("\(error)")
//		}
//	}
//
//	func testAbstractionSimpleBinding()
//	{
//		let lambdaExpressionString = "λx.x"
//		let expression = try! Parser().parse(sequence: lambdaExpressionString).get()
//		let expectedResult = #"{"_lambda":"x","body":{"bound":"x"},"redexCount":0}"#
//		let encoder = JSONEncoder()
//		encoder.outputFormatting = [.sortedKeys]
//		do
//		{
//			let string = try String(bytes: encoder.encode(Expression.CodingWrapper(expression)),
//									encoding: .utf8)!
//			XCTAssert(string == expectedResult, "Wrong encoding: '\(string)'")
//		}
//		catch
//		{
//			XCTFail("\(error)")
//		}
//	}
//
//	func testRedexDetection()
//	{
//		let lambdaExpressionString = "(λx.x) y"
//		let expression = try! Parser().parse(sequence: lambdaExpressionString).get()
//		let expectedResult = #"{"redex":[{"_lambda":"x","body":{"bound":"x"},"redexCount":0},"y"],"redexCount":1}"#
//		let encoder = JSONEncoder()
//		encoder.outputFormatting = [.sortedKeys]
//		do
//		{
//			let string = try String(bytes: encoder.encode(Expression.CodingWrapper(expression)),
//									encoding: .utf8)!
//			XCTAssert(string == expectedResult, "Wrong encoding: '\(string)'")
//		}
//		catch
//		{
//			XCTFail("\(error)")
//		}
//	}

}
