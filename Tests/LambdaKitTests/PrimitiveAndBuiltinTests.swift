//
//  PrimitiveAndBuiltinTests.swift
//  
//
//  Created by Jeremy Pereira on 16/04/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import LambdaKit

class PrimitiveAndBuiltinTests: XCTestCase
{
    func testNumeralSuccessor() throws
	{
		let three = Numeral(value: 3)
		let four = Numeral(value: 4)
		let builtIn = BuiltInManager.default["numeralSuccessor"]

		if let result = builtIn(three)
		{
			XCTAssert(result == four)
		}
		else
		{
			XCTFail("Failed to get the result for three")
		}

		let apple = FreeVariable("apple")
		if let result = builtIn(apple)
		{
			XCTFail("Got non null result: \(result.description)")
		}
    }

	func testNoBuiltIn()
	{
		let three = Numeral(value: 3)
		let builtIn = BuiltInManager.default["hgdhghgfddghghd"]

		if let result = builtIn(three)
		{
			XCTFail("Got non null result: \(result.description)")
		}
	}

	let parser = Parser(enableNumerals: true)
	let numeralSuccessor = "numeralSuccessor"

	func testPrimitiveNumeralSuccessor()
	{
		do
		{
			let realSucc = try parser.parse(sequence: "λn.λf.λx.n f (f x)").get().asAbstraction!
			let succ = PrimitiveAbstraction(name: numeralSuccessor,
											builtIn: BuiltInManager.default[numeralSuccessor],
											alternate: realSucc)
			let result1 = succ.substituting(argument: Numeral(value: 3))
			XCTAssert(result1 == Numeral(value: 4))
			let result2 = succ.substituting(argument: FreeVariable("apple"))
			let expected = try parser.parse(sequence: "λf.λx.apple f (f x)").get()
			XCTAssert(result2.isAlphaEquivalent(to: expected))
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPrimitiveDescription()
	{
		do
		{
			let realSucc = try parser.parse(sequence: "λn.λf.λx.n f (f x)").get().asAbstraction!
			let p = PrimitiveAbstraction(name: numeralSuccessor,
										 builtIn: BuiltInManager.default[numeralSuccessor],
										 alternate: realSucc)
			let expectedDesc = "@primitive(\(numeralSuccessor) \(realSucc.description))"
			XCTAssert(p.description == expectedDesc,
					  "Wrong description, expected:\n\(expectedDesc)\ngot\n\(p.description)")
			let application  = apply("apple", p)
			let expectedDesc2 = "apple \(p.description)"
			XCTAssert(application.description == expectedDesc2,
					  "Wrong description, expected:\n\(expectedDesc2)\ngot\n\(application.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testPrimitiveEquality()
	{
		do
		{
			let realSucc = try parser.parse(sequence: "λn.λf.λx.n f (f x)").get().asAbstraction!
			let p = PrimitiveAbstraction(name: numeralSuccessor,
										 builtIn: BuiltInManager.default[numeralSuccessor],
										 alternate: realSucc)
			let q = PrimitiveAbstraction(name: numeralSuccessor,
										 builtIn: BuiltInManager.default[numeralSuccessor],
										 alternate: realSucc)
			let r = p
			XCTAssert(p == r)
			XCTAssert(p == p)
			XCTAssert(p != q)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPrimitiveFreeVariables()
	{
		do
		{
			let realExpression = try parser.parse(sequence: "λn.λf.λx.n banana apple f (f x)").get().asAbstraction!
			let p = PrimitiveAbstraction(name: "doesNotMatter",
										 builtIn: BuiltInManager.default["doesNotMatter"],
										 alternate: realExpression)
			let pFreeVariables = p.freeVariableNames
			let realFreeVariables = realExpression.freeVariableNames
			XCTAssert(pFreeVariables == realFreeVariables)
			XCTAssert(pFreeVariables.count > 0)

			XCTAssert(p.count(freeVariable: FreeVariable("apple")) == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPrimitiveBetaReduction()
	{
		do
		{
			let realSucc = try parser.parse(sequence: "λn.λf.λx.n f (f x)").get().asAbstraction!
			let succ = PrimitiveAbstraction(name: numeralSuccessor,
											builtIn: BuiltInManager.default[numeralSuccessor],
											alternate: realSucc)
			let testApply = apply(succ, Numeral(value: 3))
			let result = testApply.betaReduced()
			XCTAssert(result == Numeral(value: 4))

			let applep = Parameter("apple")
			let apple = BoundVariable(applep)
			let bananap = Parameter("banana")
			let banana = BoundVariable(bananap)

			let realFunc = ConcreteAbstraction.create(parameter: bananap, in: apply(apple, banana))
			let primitive = PrimitiveAbstraction(name: numeralSuccessor,
												 builtIn: BuiltInManager.default[numeralSuccessor],
												 alternate: realFunc)
			let outerFunc = ConcreteAbstraction.create(parameter: applep, in: primitive)
			let result2 = apply(outerFunc, "pear").betaReduced()
			let expected = try parser.parse(sequence: "λbanana.pear banana").get()
			XCTAssert(result2.isAlphaEquivalent(to: expected), "expected:\n\(expected)\ngot\n\(result2.description)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testPrimitiveAlphaEquivalence()
	{
		let bananap = Parameter("banana")
		let banana = BoundVariable(bananap)

		let realFunc = ConcreteAbstraction.create(parameter: bananap, in: apply("apple", banana))
		print(realFunc.description)
		let primitive = PrimitiveAbstraction(name: "nothing",
											 builtIn: BuiltInManager.default["nothing"],
											 alternate: realFunc)
		XCTAssert(primitive.isAlphaEquivalent(to: realFunc))
		XCTAssert(realFunc.isAlphaEquivalent(to: primitive))

		let pearp = Parameter("pear")
		let pear = BoundVariable(pearp)
		let pearFunc = ConcreteAbstraction.create(parameter: pearp, in: apply("apple", pear))
		print(pearFunc.description)
		let primitive2 = PrimitiveAbstraction(name: "nothing",
											  builtIn: BuiltInManager.default["nothing"],
											  alternate: pearFunc)
		XCTAssert(primitive2.isAlphaEquivalent(to: realFunc))
		XCTAssert(primitive2.isAlphaEquivalent(to: pearFunc))
		XCTAssert(realFunc.isAlphaEquivalent(to: primitive2))
		XCTAssert(pearFunc.isAlphaEquivalent(to: primitive2))
		XCTAssert(primitive.isAlphaEquivalent(to: primitive2))
	}

	func testParsing() throws
	{
		var parser = Parser(enableNumerals: true, enableAnnotations: false)
		let testString = "@primitive(numeralSuccessor  λn.λf.λx.n f (f x))"
		let expression = try parser.parse(sequence: testString).get()
		XCTAssert(expression is Application)

		parser.enableAnnotations = true
		let expression2 = try parser.parse(sequence: testString).get()
		XCTAssert(expression2 is PrimitiveAbstraction, "\(expression2.description) is not a primitive")
	}


	func testNumeralSuccessorReduction() throws
	{
		let p = Parser(enableNumerals: true, enableAnnotations: true)
		let testString = "@primitive(numeralSuccessor  λn.λf.λx.n f (f x)) 3"
		let expression = try p.parse(sequence: testString).get()
		print(expression.description)
		let result = expression.betaReduced()
		XCTAssert(result == Numeral(value: 4), "Invalid result \(result.description)")
	}

	func testNumeralPredecessorReduction() throws
	{
		let p = Parser(enableNumerals: true, enableAnnotations: true)
		// The alternate is wrong - gives successor, but that's ok for a test
		let testString = "@primitive(numeralPredecessor  λn.λf.λx.n f (f x)) 3"
		let expression = try p.parse(sequence: testString).get()
		print(expression.description)
		let result = expression.betaReduced()
		XCTAssert(result == Numeral(value: 2), "Invalid result \(result.description)")
		let testString2 = "@primitive(numeralPredecessor  λn.λf.λx.n f (f x)) 0"
		let expression2 = try p.parse(sequence: testString2).get()
		print(expression2.description)
		let result2 = expression2.betaReduced().betaReduced().betaReduced()
		XCTAssert(result2.isAlphaEquivalent(to: Numeral(value: 1)) , "Invalid result \(result2.description)")
	}
}
