// swift-tools-version:5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LambdaCalculus",
	platforms: [
		.macOS(.v11),
	],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LambdaKit",
            targets: ["LambdaKit"]),
		.executable(name: "lambdacalc", targets: ["lambdacalc"]),
   ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "21.3.2"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.2.2"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/linenoise-swift-utf8.git", from: "1.0.3"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/cltest.git", from: "0.2.3"),
	],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "LambdaKit",
            dependencies: [
				"Toolbox",
				.product(name: "CLKit", package: "CLTest")]),
        .testTarget(
            name: "LambdaKitTests",
            dependencies: ["LambdaKit"],
			resources: [.copy("Resources/top1.txt"), .copy("Resources/top2.txt"), .copy("Resources/lib")]),
		.executableTarget(
			name: "lambdacalc",
			dependencies: ["LambdaKit",
						   .product(name: "LineNoise", package: "linenoise-swift-utf8"),
						   .product(name: "Toolbox", package: "Toolbox"),
						   .product(name: "ArgumentParser", package: "swift-argument-parser"),
						   .product(name: "CLKit", package: "CLTest"),
			]),
    ]
)
